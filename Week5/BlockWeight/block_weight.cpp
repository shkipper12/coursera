#include <iostream>

/* 
    Вычислите суммарную массу имеющих форму прямоугольного параллелепипеда 
    блоков одинаковой плотности, но разного размера.
*/

int main()
{
    int N;
    uint64_t R;
    std::cin >> N >> R;

    uint64_t v_sum = 0;
    while (N > 0)
    {
        uint64_t W,H,D;
        std::cin >> W >> H >> D;

        uint64_t V = W * H * D;
        v_sum += V;
        --N;
    }

    std::cout << v_sum * R << '\n';
}