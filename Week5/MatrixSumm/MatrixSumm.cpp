#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

using namespace std;

// Реализуйте здесь
// * класс Matrix
class Matrix
{
public:
    Matrix()
    {
        m_Mat.resize(0);
    }

    Matrix(int rows, int cols)
    {
        if (rows < 0 || cols < 0) {
            throw out_of_range("Rows and cols can't be < 0");
        }
        
        m_Mat.resize(rows); // set rows
        for (auto& r : m_Mat){
            r.resize(cols); // set cols
        }
    }

    void Reset(int rows, int cols)
    {
        if (rows < 0 || cols < 0) {
            throw out_of_range("Rows and cols can't be < 0");
        }

        m_Mat.resize(rows); // set rows
        for (auto& r : m_Mat){
            r.assign(cols, 0); // set cols
        }
    }

    int At(int row, int col) const
    {
        return m_Mat.at(row).at(col);
    }

    int& At(int row, int col)
    {
        return m_Mat.at(row).at(col);
    }

    int GetNumRows() const
    {
        return m_Mat.size();
    }
    
    int GetNumColumns() const
    {
        if (m_Mat.size() > 0) {
            return m_Mat.at(0).size();
        }
        else {
            return 0;
        }
    }

    friend bool operator==(const Matrix& lhs, const Matrix& rhs);

private:
    vector<vector<int>> m_Mat;
};

// * оператор ввода для класса Matrix из потока istream
istream& operator>>(istream& in, Matrix& rhs)
{
    int r, c;
    if (in >> r && in >> c) {
        rhs.Reset(r, c);
        for (int i = 0; i < r; ++i) {
            for (int y = 0; y < c; ++y) {
                int v;
                if (in >> v) {
                    rhs.At(i, y) = v;
                }
                else {
                    throw invalid_argument("Value must be int");
                } 
            }
        }
    }

    return in;
}

// * оператор вывода класса Matrix в поток ostream
ostream& operator<<(ostream& out, const Matrix& rhs)
{
    out << rhs.GetNumRows() << ' ' << rhs.GetNumColumns() << '\n';
    for (int i = 0; i < rhs.GetNumRows(); ++i) {
        for (int y = 0; y < rhs.GetNumColumns(); ++y) {
            if (y == 0) {
                out << rhs.At(i, y);
            }
            else {
                out << ' ' << rhs.At(i, y);
            }
        }
        out << '\n';
    }

    return out;
}

// * оператор проверки на равенство двух объектов класса Matrix
bool operator==(const Matrix& lhs, const Matrix& rhs)
{
    return lhs.m_Mat == rhs.m_Mat;

}
// * оператор сложения двух объектов класса Matrix
Matrix operator+ (const Matrix& lhs, const Matrix& rhs)
{
    if (lhs.GetNumRows() != rhs.GetNumRows()) {
        throw invalid_argument("Mismatched number of rows");
    }

    if (lhs.GetNumColumns() != rhs.GetNumColumns()) {
        throw invalid_argument("Mismatched number of columns");
    }

    Matrix m {lhs.GetNumRows(), lhs.GetNumColumns()};
    for (int i = 0; i < m.GetNumRows(); ++i) {
        for (int y = 0; y < m.GetNumColumns(); ++y) {
            m.At(i, y) = lhs.At(i, y) + rhs.At(i, y);
        }
    }

    return m;
}

int main() {
    
    {
        try {
            Matrix one (0, -3);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix two (-10, 3);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix three (-10, -3);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }
    }

    {
        try {
            Matrix one (1, 4);
            one.Reset(0, -1);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            one.Reset(-2, -1);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            one.Reset(-2, 1);
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }
    }

    {
        try {
            Matrix one (1, 4);
            cout << "value at (0, 0) - " << one.At(0, 0) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (0, 3) - " << one.At(0, 3) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (-1, 3) - " << one.At(-1, 3) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (1, -3) - " << one.At(1, -3) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (-1, -3) - " << one.At(-1, -3) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (2, 1) - " << one.At(2, 1) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 4);
            cout << "value at (1, 4) - " << one.At(1, 4) << '\n';
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }
    }

    {
        try {
            Matrix one (1, 2);
            Matrix two (1, 2);

            cout << "(1, 2) + (1, 2):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            const Matrix one (1, 2);
            const Matrix two (1, 2);

            cout << "(1, 2) + (1, 2):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (2, 2);
            Matrix two (2, 2);

            cout << "(2, 2) + (2, 2):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (0, 2);
            Matrix two (0, 2);

            cout << "(0, 2) + (0, 2):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 0);
            Matrix two (1, 0);

            cout << "(1, 0) + (1, 0):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (0, 0);
            Matrix two (0, 0);

            cout << "(0, 0) + (0, 0):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one;
            Matrix two;

            cout << "() + ():\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (1, 2);
            Matrix two (2, 1);

            cout << "(1, 2) + (2, 1):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }

        try {
            Matrix one (2, 1);
            Matrix two (1, 2);

            cout << "(2, 1) + (1, 2):\n";
            cout << one + two;
        }
        catch (exception& ex) {
            cout << "Error: " << ex.what() << '\n';
        }
    }

    {
        Matrix one (1, 4);
        Matrix two (1, 4);
        if (one == two) {
            cout << "Matrixes are the same\n";
        }
        else {
            cout << "Matrixes are not the same\n";
        }

        two.Reset(2, 4);
        if (one == two) {
            cout << "Matrixes are the same\n";
        }
        else {
            cout << "Matrixes are not the same\n";
        }

        one.Reset(2, 4);
        if (one == two) {
            cout << "Matrixes are the same\n";
        }
        else {
            cout << "Matrixes are not the same\n";
        }
    }

    return 0;
}