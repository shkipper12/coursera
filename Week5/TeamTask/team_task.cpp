#include <iostream>
#include <map>
#include <string>

// Перечислимый тип для статуса задачи
enum class TaskStatus {
    NEW,          // новая
    IN_PROGRESS,  // в разработке
    TESTING,      // на тестировании
    DONE          // завершена
};

// Объявляем тип-синоним для map<TaskStatus, int>,
// позволяющего хранить количество задач каждого статуса
using TasksInfo = std::map<TaskStatus, int>;

class TeamTasks 
{
public:
    // Получить статистику по статусам задач конкретного разработчика
    const TasksInfo& GetPersonTasksInfo(const std::string& person) const
    {
        return m_TeamTasks.at(person);
    }
    
    // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
    void AddNewTask(const std::string& person)
    {
        ++m_TeamTasks[person][TaskStatus::NEW];
    }

    void UpdateTasksInfo(const std::string& person, const TasksInfo& updatedTasks)
    {
        for (const auto& [task_s, num] : updatedTasks) {

            switch (task_s)
            {
            case TaskStatus::IN_PROGRESS:
                m_TeamTasks[person][TaskStatus::NEW] -= num;
                if (m_TeamTasks[person][TaskStatus::NEW] == 0) {
                    m_TeamTasks[person].erase(TaskStatus::NEW);
                }
                m_TeamTasks[person][TaskStatus::IN_PROGRESS] += num;
                break;
            case TaskStatus::TESTING:
                m_TeamTasks[person][TaskStatus::IN_PROGRESS] -= num;
                if (m_TeamTasks[person][TaskStatus::IN_PROGRESS] == 0) {
                    m_TeamTasks[person].erase(TaskStatus::IN_PROGRESS);
                }
                m_TeamTasks[person][TaskStatus::TESTING] += num;
                break;
            case TaskStatus::DONE:
                m_TeamTasks[person][TaskStatus::TESTING] -= num;
                if (m_TeamTasks[person][TaskStatus::TESTING] == 0) {
                    m_TeamTasks[person].erase(TaskStatus::TESTING);
                }
                m_TeamTasks[person][TaskStatus::DONE] += num;
            default:
                break;
            }
        }
    }
    
    // Обновить статусы по данному количеству задач конкретного разработчика,
    std::tuple<TasksInfo, TasksInfo> PerformPersonTasks(const std::string& person, int task_count)
    {
        if (!m_TeamTasks.count(person))
            return {};
        
        TasksInfo updatedTasks, untochedTasks;
        
        int cur_task = 0;
        for (const auto& [task_s, num] : m_TeamTasks[person])
        {
            if (task_s == TaskStatus::DONE)
                break;

            // get updated tasks 
            int untoched_num = 0;
            bool read = false;
            for (int i = 0; (i < num) && (task_count > cur_task); ++i)
            {
                read = true;
                switch (task_s)
                {
                case TaskStatus::NEW:
                    ++updatedTasks[TaskStatus::IN_PROGRESS];
                    break;
                case TaskStatus::IN_PROGRESS:
                    ++updatedTasks[TaskStatus::TESTING];
                    break;
                case TaskStatus::TESTING:
                    ++updatedTasks[TaskStatus::DONE];
                    break;
                default:
                    break;
                }

                ++cur_task;

                if (task_count == cur_task) {
                    untoched_num = num - i - 1;
                }
            }

            // get untoched tasks
            if (read && untoched_num > 0)
            {
                untochedTasks[task_s] = untoched_num;
            }
            else if (!read && num > 0) {
                untochedTasks[task_s] = num;
            }
        } 


        UpdateTasksInfo(person, updatedTasks);
        return {updatedTasks, untochedTasks};
    }

private:
    std::map<std::string, TasksInfo> m_TeamTasks {{}};
};

// Принимаем словарь по значению, чтобы иметь возможность
// обращаться к отсутствующим ключам с помощью [] и получать 0,
// не меняя при этом исходный словарь
void PrintTasksInfo(TasksInfo tasks_info) {
    std::cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
        ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
        ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
        ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << '\n';
}

int main() {
    // TeamTasks tasks;

    // // AddNewTasks Alice 5
    // for (int i = 0; i < 5; ++i) {
    //     tasks.AddNewTask("Alice");
    // }

    // // PerformPersonTasks Alice 5
    // TasksInfo updated_tasks, untouched_tasks;
    // std::tie(updated_tasks, untouched_tasks) =
    //     tasks.PerformPersonTasks("Alice", 5);

    // std::cout << "Updated Alice's tasks: ";
    // PrintTasksInfo(updated_tasks);
    // std::cout << "Untouched Alice's tasks: ";
    // PrintTasksInfo(untouched_tasks);

    // //PerformPersonTasks Alice 5
    // std::tie(updated_tasks, untouched_tasks) =
    //     tasks.PerformPersonTasks("Alice", 5);
    
    // std::cout << "Updated Alice's tasks: ";
    // PrintTasksInfo(updated_tasks);
    // std::cout << "Untouched Alice's tasks: ";
    // PrintTasksInfo(untouched_tasks);

    // // PerformPersonTasks Alice 1
    // std::tie(updated_tasks, untouched_tasks) =
    //     tasks.PerformPersonTasks("Alice", 1);

    // std::cout << "Updated Alice's tasks: ";
    // PrintTasksInfo(updated_tasks);
    // std::cout << "Untouched Alice's tasks: ";
    // PrintTasksInfo(untouched_tasks);

    // //AddNewTasks Alice 5
    // for (int i = 0; i < 5; ++i) {
    //     tasks.AddNewTask("Alice");
    // }
    
    // // PerformPersonTasks Alice 2
    // std::tie(updated_tasks, untouched_tasks) =
    //     tasks.PerformPersonTasks("Alice", 2);
    
    // std::cout << "Updated Alice's tasks: ";
    // PrintTasksInfo(updated_tasks);
    // std::cout << "Untouched Alice's tasks: ";
    // PrintTasksInfo(untouched_tasks);

    // // GetPersonTasksInfo Alice
    // std::cout << "Alice's tasks: ";
    // PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));

    // // PerformPersonTasks Alice 4
    // std::tie(updated_tasks, untouched_tasks) =
    //     tasks.PerformPersonTasks("Alice", 4);
    
    // std::cout << "Updated Alice's tasks: ";
    // PrintTasksInfo(updated_tasks);
    // std::cout << "Untouched Alice's tasks: ";
    // PrintTasksInfo(untouched_tasks);

    // // GetPersonTasksInfo Alice
    // std::cout << "Alice's tasks: ";
    // PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));

    
    TeamTasks tasks;
    tasks.AddNewTask("Ilia");
    for (int i = 0; i < 3; ++i) {
        tasks.AddNewTask("Ivan");
    }
    std::cout << "Ilia's tasks: ";
    PrintTasksInfo(tasks.GetPersonTasksInfo("Ilia"));
    std::cout << "Ivan's tasks: ";
    PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
    
    TasksInfo updated_tasks, untouched_tasks;
    
    tie(updated_tasks, untouched_tasks) =
        tasks.PerformPersonTasks("Ivan", 2);
    std::cout << "Updated Ivan's tasks: ";
    PrintTasksInfo(updated_tasks);
    std::cout << "Untouched Ivan's tasks: ";
    PrintTasksInfo(untouched_tasks);
    
    tie(updated_tasks, untouched_tasks) =
        tasks.PerformPersonTasks("Ivan", 2);
    std::cout << "Updated Ivan's tasks: ";
    PrintTasksInfo(updated_tasks);
    std::cout << "Untouched Ivan's tasks: ";
    PrintTasksInfo(untouched_tasks);


  return 0;
}