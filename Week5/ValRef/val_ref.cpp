#include <map>
#include <iostream>
#include <string>

template<typename T, typename U>
U& GetRefStrict(std::map<T,U>& m, T key)
{
    if (!m.count(key))
    {
        throw std::runtime_error("Key not found");
    }

    return m[key];
}

int main()
{
    std::map<int, std::string> m = {{0, "value"}};
    std::string& item = GetRefStrict(m, 1);
    item = "newvalue";

    std::cout << m[0] << '\n';
}