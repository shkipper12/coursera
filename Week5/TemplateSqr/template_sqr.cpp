#include <iostream>
#include <vector>
#include <utility>
#include <map>

template <typename T>
T Sqr(T v);

template <typename First, typename Second>
std::pair<First, Second> Sqr(const std::pair<First, Second>& p);

template <typename T>
std::vector<T> Sqr(const std::vector<T>& vi);

template <typename Key, typename Value>
std::map<Key, Value> Sqr(const std::map<Key, Value>& m);

template <typename First, typename Second>
std::pair<First, Second> Sqr(const std::pair<First, Second>& p);

template <typename T>
T Sqr(T v)
{
    return v * v;
}

template <typename T>
std::vector<T> Sqr(const std::vector<T>& vi)
{
    std::vector<T> res;
    for (const T& v : vi)
    {
        res.push_back(Sqr(v));
    }
    return res;
}

template <typename First, typename Second>
std::pair<First, Second> Sqr(const std::pair<First, Second>& p)
{
    return {Sqr(p.first), Sqr(p.second)};
}

template <typename Key, typename Value>
std::map<Key, Value> Sqr(const std::map<Key, Value>& m)
{
    std::map<Key, Value> res;
    for (const auto& [key, val] : m)
    {
        res[key] = Sqr(val);
    }
    return res;
}

int main()
{
    // Пример вызова функции
    std::vector<int> v = {1, 2, 3};
    std::cout << "vector:";
    for (int x : Sqr(v)) {
        std::cout << ' ' << x;
    }
    std::cout << '\n';

    std::map<int, std::pair<int, int>> map_of_pairs = {
        {4, {2, 2}},
        {7, {4, 3}}
    };
    std::cout << "map of pairs:" << '\n';
    for (const auto& x : Sqr(map_of_pairs)) {
        std::cout << x.first << ' ' << x.second.first << ' ' << x.second.second << '\n';
    }

    std::vector<std::pair<int, int>> vp = {{1, 1}, {2, 2}, {3, 3}};
    std::cout << "vector with pairs:";
    for (const auto& x : Sqr(vp)) {
        std::cout << x.first << ' ' << x.second << ' ';
    }
    std::cout << '\n';

    std::vector<std::map<int, std::pair<int, int>>> vm {{{1, {2,2}}, {2, {3,3}}}};
    std::cout << "vector with map of pairs:";
    for (const auto& maps : Sqr(vm)) {
        for (const auto& x : maps) {
            std::cout << x.first << ' ' << x.second.first << ' ' << x.second.second << '\n';
        }
    }
    
    std::cout << '\n';


}