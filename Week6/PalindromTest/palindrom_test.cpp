#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

bool IsPalindrom(const string& str) {
  // Вы можете вставлять сюда различные реализации функции,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный
    for (int i = 0; i < str.size() / 2; ++i) {
        if (str[i] != str[str.size() - 1 - i])
            return false;
    }

    return true;
}

void TestPalindrom()
{
    Assert(IsPalindrom("x"), "x");
    Assert(IsPalindrom("S"), "x");
    Assert(!IsPalindrom("xS"), "xS");
    Assert(!IsPalindrom("Sx"), "Sx");
    Assert(!IsPalindrom("xs"), "xs");
    Assert(IsPalindrom(""), "empty");
    Assert(IsPalindrom("x x"), "x x");
    Assert(!IsPalindrom("x  "), "x x");
    Assert(IsPalindrom("madam"), "madam");
    Assert(IsPalindrom("level"), "level");
    Assert(IsPalindrom("wasitacaroracatisaw"), "wasitacaroracatisaw");

    AssertEqual(IsPalindrom(""), true, "0");
	AssertEqual(IsPalindrom("1"), true, "1");
	AssertEqual(IsPalindrom("22"), true, "22");
	AssertEqual(IsPalindrom("333"), true, "333");
	AssertEqual(IsPalindrom("4444"), true, "4444");
	AssertEqual(IsPalindrom("55055"), true, "55055");

	AssertEqual(IsPalindrom("madam"), true, "madam");
	AssertEqual(IsPalindrom("level"), true, "level");
	AssertEqual(IsPalindrom("wasitacaroracatisaw"), true, "wasitacaroracatisaw");

	AssertEqual(IsPalindrom(" madam "), true, "= madam =");
	AssertEqual(IsPalindrom("  madam  "), true, "=  madam  =");
	AssertEqual(IsPalindrom(" ma - d - am "), true, "= ma - d - am =");

	AssertEqual(IsPalindrom("ff"), true, "ff");
	AssertEqual(IsPalindrom("12aa21"), true, "12aa21");

	AssertEqual(IsPalindrom("madam madam"), true, "madam madam");
	AssertEqual(IsPalindrom("madam  madam"), true, "madam  madam");
	AssertEqual(IsPalindrom("madam   madam"), true, "madam   madam");
	AssertEqual(IsPalindrom("madam madam madam"), true, "madam madam madam");

	AssertEqual(IsPalindrom("Amama"), false, "Amama");															// false
	AssertEqual(IsPalindrom("Sator Arepo Tenet Opera Rotas"), false, "Sator Arepo Tenet Opera Rotas");			// false

	AssertEqual(IsPalindrom("12aabb"), false, "12aabb");

    // игнорируется первый или последний символ;
	AssertEqual(IsPalindrom("qweY_Yew"), false, "qweY_Yew");
	AssertEqual(IsPalindrom("weY_Yewq"), false, "weY_Yewq");
	AssertEqual(IsPalindrom("ZweY_YewX"), false, "ZweY_YewX");

	// сравнение соответствующих символов завершается не в середине строки, а раньше;
	AssertEqual(IsPalindrom("123ab321"), false, "123ab321");

	// игнорируются пробелы.
	AssertEqual(IsPalindrom("1 23 321"), false, "1 23 321");
	AssertEqual(IsPalindrom("1 23 3 21"), false, "1 23 3 21");


    Assert(IsPalindrom(""), "empty string is a palindrome");
    Assert(IsPalindrom("a"), "one letter string is a palindrome");
    Assert(IsPalindrom("abba"), "abba is a palindrome");
    Assert(IsPalindrom("abXba"), "abXba is a palindrome");
    Assert(IsPalindrom("a b X b a"), "`a b X b a` is a palindrome");
    Assert(IsPalindrom("  ABBA  "), "`  ABBA  ` is a palindrome");

    Assert(!IsPalindrom("XabbaY"), "XabbaY is not a palindrome");
    Assert(!IsPalindrom("abXYba"), "abXYba is not a palindrome");
    Assert(!IsPalindrom("Xabba"), "Xabba is not a palindrome");
    Assert(!IsPalindrom("abbaX"), "abbaX is not a palindrome");
    Assert(
    !IsPalindrom("was it a car or a cat i saw"),
    "`was it a car or a cat i saw` is not a palindrome because spaces do not match"
    );
    Assert(!IsPalindrom("ABBA   "), "`ABBA   ` is not a palindrome");
    Assert(!IsPalindrom("  ABBA"), "`  ABBA` is not a palindrome");
}

int main() {
  TestRunner runner;
  // добавьте сюда свои тесты
  runner.RunTest(TestPalindrom, "TestPalindrom");
  return 0;
}
