#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
      os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

vector<string> FormNamesHistory(int year, const map<int, string>& data)
{
	vector<string> result;
	int limit = data.begin() -> first;
	for(int i = year; i >= limit; --i) {
  		if(data.count(i))
 			result.push_back(data.at(i));
  	}
  	return result;
}

string FindName(const int year, const map<int, string>& data)
{
  	string name;
  	for (const auto& item : data) {
	    if (item.first <= year)
	    	name = item.second;
	    else
			break;
	}

	return name;
}

string GetHistoryNames(const vector<string>& data)
{
	string result;// = " (";
	string check = data[0];
	unsigned int limit = data.size()-1;
	for(unsigned int i = 1; i < limit; ++i)	{
		if(check != data[i]) {
			result += data[i] + ", ";
			check = data[i];
		}
	}

	if(data[limit] != check)
		result += data[limit];

	if(!result.empty()) {
		return " (" + result + ")";
	}
	//result += ")";
	return result;
}

class Person 
{
public:
    Person()
    {
        year_of_birth = 0;
        firstName[year_of_birth] = "";
        lastName[year_of_birth ] = "";
    }
    Person(const string& name, const string& surname, int year) {
        year_of_birth = year;
        firstName[year] = name;
        lastName[year] = surname;
    }

    void ChangeFirstName(int year, const string& first_name) 
    {
        // добавить факт изменения имени на first_name в год year
        if (year >= GetYearOfBirth()) {
            firstName[year] = first_name;
        }
    }


    void ChangeLastName(int year, const string& last_name)
    {
        // добавить факт изменения фамилии на last_name в год year
        if (year >= GetYearOfBirth()) {
            lastName[year] = last_name;
        }
    }

    string GetFullName(int year) const {
    // получить имя и фамилию по состоянию на конец года year
        if (year < GetYearOfBirth()) {
            return "No person";
        }
    
        string name = FindName(year, firstName);
        string surname = FindName(year, lastName);

        if(!name.empty() && !surname.empty())
            return name + " " + surname;
        else if(!name.empty())
            return name + " with unknown last name";
        else if(!surname.empty())
            return surname + " with unknown first name";
        else return "Incognito";
    }

    string GetFullNameWithHistory(int year) const {
        if (year < GetYearOfBirth()) {
            return "No person";
        }
        
        vector<string> names = FormNamesHistory(year, firstName);
        vector<string> surnames = FormNamesHistory(year, lastName);
        string result;

        if(!names.empty() && !surnames.empty())	{
            result = names[0];

            if(names.size() > 1) {
                result += GetHistoryNames(names);
            }

            result += (" " + surnames[0]);

            if(surnames.size() > 1) {
                result += GetHistoryNames(surnames);
            }
            return result;
        }
        else if(!names.empty()) {
            result = names[0];

            if(names.size() > 1) {
                result += GetHistoryNames(names);
            }
            result += " with unknown last name";
        }
        else if(!surnames.empty()) {
            result = surnames[0];
            if(surnames.size() > 1) {
                result += GetHistoryNames(surnames);
            }
            result += " with unknown first name";
        }
        else result = "Incognito";

        return result;
    }

    int GetYearOfBirth() const {
        return year_of_birth;
    }

private:
    // приватные поля
	map<int, string> firstName;
	map<int, string> lastName;
    int year_of_birth;
};

void TestPredefinedLastNameFirst() {
    Person person;

    person.ChangeLastName(1965, "Sergeeva");
    person.ChangeFirstName(1967, "Polina");

    AssertEqual(person.GetFullName(1964), "Incognito");
    AssertEqual(person.GetFullName(1966), "Sergeeva with unknown first name");
    AssertEqual(person.GetFullName(1968), "Polina Sergeeva");
}

void TestPredefined() {
    Person person;

    person.ChangeFirstName(1965, "Polina");
    person.ChangeLastName(1967, "Sergeeva");

    AssertEqual(person.GetFullName(1964), "Incognito");
    AssertEqual(person.GetFullName(1966), "Polina with unknown last name");
    AssertEqual(person.GetFullName(1968), "Polina Sergeeva");

    person.ChangeFirstName(1969, "Appolinaria");
    AssertEqual(person.GetFullName(1968), "Polina Sergeeva");
    AssertEqual(person.GetFullName(1969), "Appolinaria Sergeeva");
    AssertEqual(person.GetFullName(1970), "Appolinaria Sergeeva");

    person.ChangeLastName(1968, "Volkova");
    AssertEqual(person.GetFullName(1967), "Polina Sergeeva");
    AssertEqual(person.GetFullName(1968), "Polina Volkova");
    AssertEqual(person.GetFullName(1969), "Appolinaria Volkova");

}

int main() {
    TestRunner runner;
    runner.RunTest(TestPredefined, "TestPredefined");
    runner.RunTest(TestPredefinedLastNameFirst, "TestPredefinedLastNameFirst");
    return 0;
}