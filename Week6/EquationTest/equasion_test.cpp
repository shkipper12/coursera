#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

int GetDistinctRealRootCount(double a, double b, double c) {
  // Вы можете вставлять сюда различные реализации функции,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный
    int root_cnt = 0;
    if (a == 0 && b != 0) { // Bx + C = 0 
        //std::cout << ((-1 * c) / b);
        root_cnt = 1;
        // case with B == 0 has no answer
    }
    else if (a != 0) {  // Ax2 + Bx + C = 0
        int D = b*b - 4 * a * c;
        if (D == 0) { 
            //std::cout << (-1 * b) / (2 * a);
            root_cnt = 1;
        }
        else if (D > 0) {
            //std::cout << ((-1 * b + std::sqrt(D)) / (2 * a)) << ' ' << ((-1 * b - std::sqrt(D)) / (2 * a));
            root_cnt = 2;
        }
    }

    return root_cnt;
}

void TestZeroRoots()
{
    // a == 0 && b == 0
    AssertEqual(GetDistinctRealRootCount(0, 0, 1), 0);
    AssertEqual(GetDistinctRealRootCount(0, 0, -5), 0);

    // a > 0 && c > 0 && b == 0
    AssertEqual(GetDistinctRealRootCount(3, 0, 5), 0);
    AssertEqual(GetDistinctRealRootCount(-3, 0, -5), 0);

    // a < 0 && c < 0 && b == 0
    AssertEqual(GetDistinctRealRootCount(-4, 0, -2), 0);
    AssertEqual(GetDistinctRealRootCount(4, 0, 2), 0);

    // D < 0
    AssertEqual(GetDistinctRealRootCount(5, 2, 4), 0);
    AssertEqual(GetDistinctRealRootCount(10, 3, 1), 0);
}

void TestOneRoot()
{
    // a == 0
    AssertEqual(GetDistinctRealRootCount(0, 4, 1), 1);
    AssertEqual(GetDistinctRealRootCount(0, 4, 0), 1);
    AssertEqual(GetDistinctRealRootCount(0, -4, 0), 1);
    AssertEqual(GetDistinctRealRootCount(0, -4, -1), 1);
    AssertEqual(GetDistinctRealRootCount(0, 4, -1), 1);
    AssertEqual(GetDistinctRealRootCount(0, -4, 1), 1);

    // D == 0
    AssertEqual(GetDistinctRealRootCount(2, 4, 2), 1);
    AssertEqual(GetDistinctRealRootCount(1, 2, 1), 1);
}

void TestTwoRoots()
{
    // D > 0 && c == 0
    AssertEqual(GetDistinctRealRootCount(2, 4, 0), 2);
    AssertEqual(GetDistinctRealRootCount(-2, -4, 0), 2);

    // a < 0 && c > 0 && b == 0
    AssertEqual(GetDistinctRealRootCount(-4, 0, 2), 2);

    // a > 0 && c < 0 && b == 0
    AssertEqual(GetDistinctRealRootCount(4, 0, -2), 2);
}

int main() {
  TestRunner runner;
  runner.RunTest(TestZeroRoots, "TestZeroRoots");
  runner.RunTest(TestOneRoot, "TestOneRoot");
  runner.RunTest(TestTwoRoots, "TestTwoRoots");
  // добавьте сюда свои тесты
  return 0;
}
