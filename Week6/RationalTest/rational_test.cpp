#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

int GetGCD(int A, int B)
{
    // std method
    //return std::gcd(numerator, denominator);

    // алгоритм Евклида
    while (A > 0 && B > 0) {
        if (A > B)
            A %= B;
        else
            B %= A;
    }

    return A + B;
}

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
        m_num = 0;
        m_denom = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
        if (denominator == 0) {
            throw invalid_argument("Invalid argument");
        }

        if (numerator < 0 && denominator < 0) {
            numerator *= -1;
            denominator *= -1;
            Reduce(numerator, denominator);
        }
        if (numerator < 0) {
            numerator *= -1;
            Reduce(numerator, denominator);
            m_num *= -1;
        }
        else if (denominator < 0) {
            denominator *= -1;
            Reduce(numerator, denominator);
            m_num *= -1;
        }
        else {
            Reduce(numerator, denominator);
        }
    }

    int Numerator() const {
        // Реализуйте этот метод
        return m_num;
    }

    int Denominator() const {
        // Реализуйте этот метод
        return m_denom;
    }

    friend istream& operator>>(istream& in, Rational& lhs);

private:

    void Reduce(int numerator, int denominator) 
    {
        int f = GetGCD(numerator, denominator);
        m_num = numerator / f;
        m_denom = denominator / f;
    }

    // Добавьте поля
    int m_num;
    int m_denom;
};

void TestRationalCtr()
{
    // 0/1
    Rational r1;
    AssertEqual(r1.Numerator(), 0);
    AssertEqual(r1.Denominator(), 1);
   
    Rational r2(0, 1);
    AssertEqual(r2.Numerator(), 0);
    AssertEqual(r2.Denominator(), 1);

    Rational r3(0, 4);
    AssertEqual(r3.Numerator(), 0);
    AssertEqual(r3.Denominator(), 1);

    Rational r4(0, -4);
    AssertEqual(r4.Numerator(), 0);
    AssertEqual(r4.Denominator(), 1);

    // reducing
    Rational r5(2, 4);
    AssertEqual(r5.Numerator(), 1);
    AssertEqual(r5.Denominator(), 2);

    Rational r6(-2, -4);
    AssertEqual(r6.Numerator(), 1);
    AssertEqual(r6.Denominator(), 2);

    Rational r7(-2, 4);
    AssertEqual(r7.Numerator(), -1);
    AssertEqual(r7.Denominator(), 2);

    Rational r8(2, -4);
    AssertEqual(r8.Numerator(), -1);
    AssertEqual(r8.Denominator(), 2);

    // no reducing
    Rational r9(2, 3);
    AssertEqual(r9.Numerator(), 2);
    AssertEqual(r9.Denominator(), 3);

    Rational r10(2, -3);
    AssertEqual(r10.Numerator(), -2);
    AssertEqual(r10.Denominator(), 3);

    Rational r11(-2, 3);
    AssertEqual(r11.Numerator(), -2);
    AssertEqual(r11.Denominator(), 3);

    Rational r12(-2, -3);
    AssertEqual(r12.Numerator(), 2);
    AssertEqual(r12.Denominator(), 3);

    // big num
    Rational r13(2147483647, 2147483647);
    AssertEqual(r13.Numerator(), 1);
    AssertEqual(r13.Denominator(), 1);
}

int main() {
  TestRunner runner;
  runner.RunTest(TestRationalCtr, "TestRationalCtr");
  // добавьте сюда свои тесты
  return 0;
}
