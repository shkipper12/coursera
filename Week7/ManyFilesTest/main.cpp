#include "sum_reverse_sort.h"
#include <iostream>

int main()
{
    std::cout << Sum(3, 4) << '\n';
    std::cout << Reverse("qwerty") << '\n';
    vector<int> test{1, 5, 2, 3, 10, 0, 4};
    Sort(test);
    for (const int& v : test)
    {
        std::cout << v << ' ';
    }
}