#include "query.h"

istream& operator >> (istream& is, Query& q)
{
  // Реализуйте эту функцию
  string op_code;
  is >> op_code;
  if (op_code == "NEW_BUS") {
      q.type = QueryType::NewBus;
      is >> q.bus;
      int stop_count;
      if (is >> stop_count) {
          q.stops.resize(stop_count);
          for (string& stop : q.stops) {
              is >> stop;
          }
      }
  }
  else if (op_code == "BUSES_FOR_STOP") {
      q.type = QueryType::BusesForStop;
      is >> q.stop;
  }
  else if (op_code == "STOPS_FOR_BUS") {
      q.type = QueryType::StopsForBus;
      is >> q.bus;
  }
  else if (op_code == "ALL_BUSES") {
      q.type = QueryType::AllBuses;
  }

  return is;
}