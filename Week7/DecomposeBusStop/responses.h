#pragma once
#include <map>
#include <ostream>
#include <vector>
#include <string>

using namespace std;

struct BusesForStopResponse {
  // Наполните полями эту структуру
  bool stop_valid;
  vector<string> buses;
};

ostream& operator << (ostream& os, const BusesForStopResponse& r);

struct StopsForBusResponse {
  // Наполните полями эту структуру
  bool bus_valid;
  string bus;
  vector<string> stops;
  map<string, vector<string>> stops_to_buses;
};

ostream& operator << (ostream& os, const StopsForBusResponse& r);

struct AllBusesResponse {
  // Наполните полями эту структуру
  map<string, vector<string>> buses_to_stops;
};

ostream& operator << (ostream& os, const AllBusesResponse& r);