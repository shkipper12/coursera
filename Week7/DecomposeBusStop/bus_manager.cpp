#include "bus_manager.h"

void BusManager::AddBus(const string& bus, const vector<string>& stops)
{
    // Реализуйте этот метод
    for (const auto& stop : stops) {
        stops_to_buses[stop].push_back(bus);
        buses_to_stops[bus].push_back(stop);
    }
}

BusesForStopResponse BusManager::GetBusesForStop(const string& stop) const {
    // Реализуйте этот метод
    BusesForStopResponse res;
    if (stops_to_buses.count(stop) == 0) {
      res.stop_valid = false;
    } else {
        res.stop_valid = true;
        res.buses = stops_to_buses.at(stop);
    }
    return res;
}

StopsForBusResponse BusManager::GetStopsForBus(const string& bus) const {
    // Реализуйте этот метод
    StopsForBusResponse res;
    res.bus = bus;
    if (buses_to_stops.count(bus) == 0) {
      res.bus_valid = false;
    } else {
      res.bus_valid = true;
      res.stops = buses_to_stops.at(bus);
      res.stops_to_buses = stops_to_buses;
    }
    return res;
}

AllBusesResponse BusManager::GetAllBuses() const {
    // Реализуйте этот метод
    return {buses_to_stops};
  }