#include "phone_number.h"
#include <sstream>
#include <iostream>
/* Принимает строку в формате +XXX-YYY-ZZZZZZ
     Часть от '+' до первого '-' - это код страны.
     Часть между первым и вторым символами '-' - код города
     Всё, что идёт после второго символа '-' - местный номер.
     Код страны, код города и местный номер не должны быть пустыми.
     Если строка не соответствует этому формату, нужно выбросить исключение invalid_argument. Проверять, что номер содержит только цифры, не нужно.

     Примеры:
     * +7-495-111-22-33
     * +7-495-1112233
     * +323-22-460002
     * +1-2-coursera-cpp
     * 1-2-333 - некорректный номер - не начинается на '+'
     * +7-1233 - некорректный номер - есть только код страны и города
  */
PhoneNumber::PhoneNumber(const string &international_number)
{
    stringstream ss;
    ss << international_number;
    if (ss.peek() != '+') {
        throw invalid_argument("Invalid phone number format! Must start with '+'");
    }
    
    // get country code
    ss.ignore(1);
    std::getline(ss, country_code_, '-');
    if (country_code_.size() == 0) {
        throw invalid_argument("Invalid phone number format! Failed to read country code! Only +XXX-YYY-ZZZZZZ format is allowed");
    }

    // get city code
    std::getline(ss, city_code_, '-');
    if (city_code_.size() == 0) {
        throw invalid_argument("Invalid phone number format! Failed to read city code! Only +XXX-YYY-ZZZZZZ format is allowed");
    }

    // get local num
    std::getline(ss, local_number_);
    if (local_number_.size() == 0) {
        throw invalid_argument("Invalid phone number format! Failed to read local number! Only +XXX-YYY-ZZZZZZ format is allowed");
    }
}

string PhoneNumber::GetCountryCode() const
{
    return country_code_;
}

string PhoneNumber::GetCityCode() const
{
    return city_code_;
}

string PhoneNumber::GetLocalNumber() const
{
    return local_number_;
}

string PhoneNumber::GetInternationalNumber() const
{
    return "+" + country_code_ + "-" + city_code_ + "-" + local_number_;
}