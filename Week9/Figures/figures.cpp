#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <iomanip>
#include <cmath>

using namespace std;

const double pi = 3.14;

class Figure
{
public:
  virtual string Name() const = 0;
  virtual double Perimeter() const = 0;
  virtual double Area() const = 0;

  virtual ~Figure() = default;
};

class Rect : public Figure
{
public:
  Rect(double w, double h)
    : m_W(w), m_H(h)
  {}

  string Name() const override
  {
    return "RECT";
  }
  
  double Perimeter() const override
  {
    return m_H * 2 + m_W * 2;
  }

  double Area() const override
  {
    return m_H * m_W;
  }

private:
  double m_W;
  double m_H;  
};


class Triangle : public Figure
{
public:
  Triangle(double a, double b, double c)
    : m_A(a), m_B(b), m_C(c)
  {}

  string Name() const override
  {
    return "TRIANGLE";
  }
  
  double Perimeter() const override
  {
    return (m_A + m_B + m_C);
  }

  double Area() const override
  {
    double p = Perimeter() / 2;
    return sqrt(p*(p-m_A)*(p-m_B)*(p-m_C));
  }

private:
  double m_A;
  double m_B;
  double m_C;
};

class Circle : public Figure
{
public:
  Circle(double r)
    : m_R(r)
  {}

  string Name() const override
  {
    return "CIRCLE";
  }
  
  double Perimeter() const override
  {
    return 2 * pi * m_R;
  }

  double Area() const override
  {
    return pi * m_R * m_R;
  }

private:
  double m_R;  
};

std::shared_ptr<Figure> CreateFigure(std::istream& is)
{
  string figure;
  is >> figure;

  if (figure == "RECT")
  {
    double w, h;
    is >> w >> h;
    return make_shared<Rect>(w, h);
  }
  else if (figure == "TRIANGLE")
  {
    double a, b, c;
    is >> a >> b >> c;
    return make_shared<Triangle>(a, b, c);
  }
  else if (figure == "CIRCLE")
  { 
    double r;
    is >> r;
    return make_shared<Circle>(r);
  }
  throw std::invalid_argument("invalid figure name was specified");
}

int main() {
  vector<shared_ptr<Figure>> figures;
  for (string line; getline(cin, line); ) {
    istringstream is(line);

    string command;
    is >> command;
    if (command == "ADD") {
      // Пропускаем "лишние" ведущие пробелы.
      // Подробнее об std::ws можно узнать здесь:
      // https://en.cppreference.com/w/cpp/io/manip/ws
      is >> ws;
      figures.push_back(CreateFigure(is));
    } else if (command == "PRINT") {
      for (const auto& current_figure : figures) {
        cout << fixed << setprecision(3)
             << current_figure->Name() << " "
             << current_figure->Perimeter() << " "
             << current_figure->Area() << endl;
      }
    }
  }
  return 0;
}