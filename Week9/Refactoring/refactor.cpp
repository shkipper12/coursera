#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class Person
{
public:
    Person(const string& name, const string& type)
      :  Name(name), Type(type)
    {}
    string GetName() const
    {
        return Name;
    }
    string GetType() const
    {
        return Type;
    }
    virtual void Walk(const string& destination)
    {
        cout << Type << ": " << GetName() << " walks to: " << destination << endl;
    }

    virtual ~Person() = default;
private:
    const string Name;
    const string Type;
};

class Student : public Person {
public:

    Student(const string& name, const string& favouriteSong) 
        : Person(name, "Student"), FavouriteSong(favouriteSong)
    {}

    void Learn() {
        cout << "Student: " << GetName() << " learns" << endl;
    }

    void Walk(const string& destination) override {
        cout << "Student: " << GetName() << " walks to: " << destination << endl;
        cout << "Student: " << GetName() << " sings a song: " << FavouriteSong << endl;
    }

    void SingSong() {
        cout << "Student: " << GetName() << " sings a song: " << FavouriteSong << endl;
    }

public:
    const string FavouriteSong;
};


class Teacher : public Person{
public:

    Teacher(const string& name, const string& subject) 
        : Person(name, "Teacher"), Subject(subject)
    {}

    void Teach() {
        cout << "Teacher: " << GetName() << " teaches: " << Subject << endl;
    }

public:
    const string Subject;
};


class Policeman : public Person {
public:
    Policeman(const string& name) 
        : Person(name, "Policeman")
    {}

    void Check(shared_ptr<Person> t) {
        cout << "Policeman: " << GetName() << " checks " << t->GetType() << ". " << t->GetType() << "'s name is: " << t->GetName() << endl;
    }
};

void VisitPlaces(Person* person, const vector<string>& places)
{
    for (auto p : places) {
        person->Walk(p);
    }
}

int main() {
    shared_ptr<Person> t = make_shared<Teacher>("Jim", "Math");
    shared_ptr<Person> s = make_shared<Student>("Ann", "We will rock you");
    shared_ptr<Policeman> p = make_shared<Policeman>("Bob");

    VisitPlaces(t.get(), {"Moscow", "London"});
    p->Check(s);
    VisitPlaces(s.get(), {"Moscow", "London"});
    //VisitPlaces(static_cast<Person*>(p.get()), {"Moscow", "London"});
    return 0;
}
