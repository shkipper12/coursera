#include <iostream>
#include <string>

bool IsPalindrom(std::string word) {

    for (int i = 0; i < word.size() / 2; ++i) {
        if (word[i] != word[word.size() - 1 - i])
            return false;
    }

    return true;
}

int main() {
    std::cout << IsPalindrom("madam") << '\n';
    std::cout << IsPalindrom("gentleman") << '\n';
    std::cout << IsPalindrom("X") << '\n';
    std::cout << IsPalindrom("aa") << '\n';
    std::cout << IsPalindrom("aav") << '\n';
    std::cout << IsPalindrom("avva") << '\n';
}