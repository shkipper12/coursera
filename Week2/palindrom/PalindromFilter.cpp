#include <iostream>
#include <string>
#include <vector>

bool IsPalindrom(std::string word) {

    for (int i = 0; i < word.size() / 2; ++i) {
        if (word[i] != word[word.size() - 1 - i]) {
            return false;
        }
    }

    return true;
}

std::vector<std::string> PalindromFilter(std::vector<std::string> words, int minLenght) 
{
    std::vector<std::string> filtered;
    for (auto word : words) {
        if (IsPalindrom(word) && word.size() >= minLenght) {
            filtered.push_back(word);
        }
    }
    return filtered;
}

void printVector(std::vector<std::string> vec) 
{
    for (auto x : vec) {
        std::cout << x << " ";
    }
    std::cout << "\n";
}

int main() {
    
    std::vector<std::string> test1 = PalindromFilter({"abacaba", "aba"}, 5);
    std::vector<std::string> test2 = PalindromFilter({"abacaba", "aba"}, 2);
    std::vector<std::string> test3 = PalindromFilter({"weew", "bro", "code"}, 4);

    printVector(test1);
    printVector(test2);
    printVector(test3);
}