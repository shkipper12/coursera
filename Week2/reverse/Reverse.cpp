#include <vector>
#include <iostream>

void Reverse(std::vector<int>& v)
{
    size_t length = v.size();
    for (int i = 0; i < length / 2; ++i) {
        int temp = v[i];
        v[i] = v[length - 1 - i];
        v[length - 1 - i] = temp;
    }
}

std::vector<int> Reversed(const std::vector<int>& v)
{
    size_t length = v.size();
    std::vector<int> new_vec;
    for (int i = length - 1; i >=0; --i) {
        new_vec.push_back(v[i]);
    }

    return new_vec;
}

void printVec(const std::vector<int>& v)
{
    for (auto x : v) {
        std::cout << x << ' ';
    }
    std::cout << '\n';
}

int main()
{
    std::vector<int> numbers_1 = {1, 5, 3, 4, 2};
    Reverse(numbers_1);
    printVec(numbers_1);

    std::vector<int> numbers_2 = {1, 5, 4, 2};
    Reverse(numbers_2);
    printVec(numbers_2);
    
    std::vector<int> numbers_3 = {1, 5, 4, 2};
    std::vector<int> new_vec = Reversed(numbers_3);
    printVec(new_vec);
}