#include <map>
#include <iostream>
#include <string>

std::map<char, int> GetElemSummary(const std::string& word) {
    std::map<char, int> result;
    for (const char& c : word) {
        ++result[c];
    }
    return result;
}

bool IsAnagrams(const std::string & word1, const std::string & word2)
{
    if (word1.size() != word2.size()) {
        return false;
    }
    return GetElemSummary(word1) == GetElemSummary(word2);
}

int main()
{

    int N;
    std::cin >> N;

    while (N > 0) {
        std::string s1, s2;
        std::cin >> s1 >> s2;
        std::cout << (IsAnagrams(s1, s2) ? "YES" : "NO") << '\n';

        --N;
    }

    
    // std::cout << (IsAnagrams("eat", "tea") ? "YES" : "NO") << '\n';
    // std::cout << (IsAnagrams("find", "search") ? "YES" : "NO") << '\n';
    // std::cout << (IsAnagrams("master", "stream") ? "YES" : "NO") << '\n';
    // std::cout << (IsAnagrams("a", "a") ? "YES" : "NO") << '\n';
    // std::cout << (IsAnagrams("a", "b") ? "YES" : "NO") << '\n';
    // std::cout << (IsAnagrams("aa", "aa") ? "YES" : "NO") << '\n';
    
}