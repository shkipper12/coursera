#include <iostream>
#include <vector>
#include <string>

int main()
{
    std::vector days_in_month = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int curr_month = 0;

    int Q;
    std::cin >> Q;
    std::vector<std::vector<std::string>> month_tasks(days_in_month[0]);

    while (Q > 0)
    {
        std::string op;
        std::cin >> op;

        if (op == "ADD") {
            int day;
            std::string task;
            std::cin >> day >> task;
            month_tasks[day-1].push_back(task);

        }
        else if (op == "DUMP") {
            int day;
            std::cin >> day;
           
            std::cout << month_tasks[day-1].size() << ' ';
            for (std::string t : month_tasks[day-1]) {
                std::cout << t << ' ';
            }
            std::cout << '\n';
        }
        else if (op == "NEXT") {

            // determine new month
            int new_month = curr_month + 1;
            if (new_month >= days_in_month.size())
                new_month = 0;

            if (days_in_month[new_month] > days_in_month[curr_month]) {
                month_tasks.resize(days_in_month[new_month]);               
            }
            else if (days_in_month[new_month] < days_in_month[curr_month]) {
                for (int i = days_in_month[new_month]; i < days_in_month[curr_month]; ++i) {
                    month_tasks[days_in_month[new_month] - 1].insert(std::end(month_tasks[days_in_month[new_month] - 1]), begin(month_tasks[i]), end(month_tasks[i]));
                }

                month_tasks.resize(days_in_month[new_month]);
            }

            curr_month = new_month;
        }
        else if (op == "PRINT") {
            int i = 1;
            for (auto tasks : month_tasks) {
                std::cout << i << ": ";
                for (auto t : tasks) {
                    std::cout << t << " ";
                } 
                std::cout << '\n';
                ++i;
            }
            ++Q;
        }
        else 
            std::cout << "Unknown command\n";

        --Q;
    }
}
