#include <iostream>
#include <string>
#include <vector>

void MoveStrings(std::vector<std::string>& source, std::vector<std::string>& destination) 
{
    for (auto s : source) {
        destination.push_back(s);
    }

    source.clear();
}


int main()
{
    std::vector<std::string> source = {"a", "b", "c"};
    std::vector<std::string> destination = {"z"};
    MoveStrings(source, destination);

    std::cout << "Source:";
    for (auto s : source) {
        std::cout << ' ' << s;
    }
    std::cout << '\n';
    
    std::cout << "Destination:";
    for (auto s : destination) {
        std::cout << ' ' << s;
    }
    std::cout << '\n';
}