#include <iostream>

int Factorial(int N) {
    
    if (N <= 1) {
        return 1;
    }
    else {
        return N * Factorial(N-1);
    }
}

int main() {
    std::cout << Factorial(1) << '\n';
    std::cout << Factorial(-2) << '\n';
    std::cout << Factorial(4) << '\n';
}