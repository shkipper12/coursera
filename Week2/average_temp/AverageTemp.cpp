#include <iostream>
#include <vector>

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> temp_at_days(N);
    int temp_sum = 0;
    
    // read temp at days
    for (int& temperature : temp_at_days) {
        std::cin >> temperature;
        temp_sum += temperature;
    }

    int average_temp = temp_sum / N;
    
    std::vector<int> filtered_days;

    // get days with temp >= average
    for (int i=0; i < temp_at_days.size(); ++i) {
        if (temp_at_days[i] > average_temp) {
            filtered_days.push_back(i);
        }
    }

    std::cout << filtered_days.size() << '\n';
    for (int d : filtered_days) {
        std::cout << d << ' ';
    }
}