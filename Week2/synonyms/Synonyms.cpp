#include <set>
#include <string>
#include <iostream>
#include <map>

int main()
{
    std::map<std::string, std::set<std::string>> simonims_dict;

    int Q;
    std::cin >> Q;
    while (Q > 0)
    {
        std::string op;
        std::cin >> op;

        if (op == "ADD") {
            std::string w1, w2;
            std::cin >> w1 >> w2;
            simonims_dict[w1].insert(w2);
            simonims_dict[w2].insert(w1);
        }
        else if (op == "COUNT") {
            std::string w1;
            std::cin >> w1;
            std::cout << simonims_dict[w1].size() << '\n';
        }
        else if (op == "CHECK") {
            std::string w1, w2;
            std::cin >> w1 >> w2;
            std::cout << (simonims_dict[w1].count(w2) ? "YES" : "NO") << '\n';
        }
        else {
            std::cout << "Unknown Command\n";
        }

        --Q;
    }
    
}