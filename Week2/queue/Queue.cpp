#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int main()
{
    int Q;
    std::cin >> Q;
    
    int queue_len = 0;
    std::vector<bool> queue;

    while (Q > 0)
    {
        std::string op;
        std::cin >> op;
        if (op == "WORRY") {
            int ind;
            std::cin >> ind; 
            queue[ind] = true;
        }   
        else if (op == "QUIET") {
            int ind;
            std::cin >> ind;
            queue[ind] = false;
        }
        else if (op == "COME") {
            int n;
            std::cin >> n;
            if (queue.size() == 0){
                queue.assign(n, false);
            }
            else {
                int new_size = queue.size() + n;
                queue.resize(new_size);
            }
        }
        else if (op == "WORRY_COUNT") {
            // подсчитываем количество элементов в векторе is_nervous, равных true
            std::cout << std::count(std::begin(queue), std::end(queue), true) << '\n';

        }
        else {
            std::cout << "Unknown command\n";
        }

        --Q;

    }
    
}