#include <map>
#include <string>
#include <vector>
#include <iostream>

int main()
{
    int Q;
    std::cin >> Q;

    std::map<std::string, std::vector<std::string>> buses_at_stop; // stop - buses
    std::map<std::string, std::vector<std::string>> stop_for_buses; // bus - stops

    while (Q > 0)
    {
        std::string op;
        std::cin >> op;
        if (op == "NEW_BUS") {
            std::string bus_num;
            int n;
            std::cin >> bus_num >> n;
            while (n > 0)
            {
                std::string stop;
                std::cin >> stop;
                buses_at_stop[stop].push_back(bus_num);
                stop_for_buses[bus_num].push_back(stop);
                --n;
            }
            
        }
        else if (op == "BUSES_FOR_STOP") {
            std::string stop;
            std::cin >> stop;
            if (buses_at_stop.count(stop)) {
                for (const auto& bus : buses_at_stop[stop]) {
                    std::cout << bus << ' ';
                }
                std::cout << '\n';
            }
            else {
                std::cout << "No stop\n";
            }
        }
        else if (op == "STOPS_FOR_BUS") {
            std::string bus;
            std::cin >> bus;
            if (stop_for_buses.count(bus)){
                for (const auto& stop : stop_for_buses[bus]) {
                    std::cout << "Stop " << stop << ":";
                    if (buses_at_stop[stop].size() > 1) {
                        for (const auto& other_bus : buses_at_stop[stop]) {
                            if (other_bus != bus)
                                std::cout << ' ' << other_bus;
                        }
                        std::cout << '\n';
                    }
                    else {
                        std::cout << " no interchange\n";
                    }
                }
            }
            else {
                std::cout << "No bus\n";
            }
        }
        else if (op == "ALL_BUSES") {
            if (stop_for_buses.size() > 0) {
                for (const auto& [bus, stops] : stop_for_buses) {
                    std::cout << "Bus " << bus << ":";
                    for (const auto& stop : stops) {
                        std::cout << ' ' << stop;
                    }
                    std::cout << '\n';
                }
            }
            else {
                std::cout << "No buses\n";
            }
        }
        else 
            std::cout << "Unknowm Command\n";

        --Q;
    }
}