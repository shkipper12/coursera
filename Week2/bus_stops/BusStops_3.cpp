#include <iostream>
#include <map>
#include <string>
#include <set>

int main()
{
    int Q;
    std::cin >> Q;
    std::map<std::set<std::string>, int> all_routes;
    int new_route_id = 0;
    while (Q > 0) {

        int stops_num;
        std::cin >> stops_num;
        
        std::set<std::string> route;
        for (int i = 0; i < stops_num; ++i) {
            std::string stop;
            std::cin >> stop;
            route.insert(stop);
        }
        
        if (all_routes.count(route)) {
            std::cout << "Already exists for " << all_routes[route] << '\n'; 
        }
        else {
            ++new_route_id;
            all_routes[route] = new_route_id;
            std::cout << "New bus " << new_route_id << '\n'; 
        }

        --Q;
    }
}