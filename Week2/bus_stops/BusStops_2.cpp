#include <iostream>
#include <vector>
#include <map>
#include <string>

int main() 
{
    int Q;
    std::cin >> Q;
    std::map<std::vector<std::string>, int> all_routes;
    int new_route_id = 0;
    while (Q > 0) {

        int stops_num;
        std::cin >> stops_num;

        std::vector<std::string> route(stops_num);
        for (int i = 0; i < stops_num; ++i) {
            //std::string stop;
            std::cin >> route[i];
        }

        if (all_routes.count(route)) {
            std::cout << "Already exists for " << all_routes[route] << '\n'; 
        }
        else {
            ++new_route_id;
            all_routes[route] = new_route_id;
            std::cout << "New bus " << new_route_id << '\n'; 
        }

        --Q;
    }
}