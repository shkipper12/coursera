#include <iostream>
#include <string>
#include <set>

int main()
{
    int N;
    std::cin >> N;

    std::set<std::string> unique_str;
    while (N > 0)
    {
        std::string new_str;
        std::cin >> new_str;

        unique_str.insert(new_str);

        --N;
    }

    std::cout << unique_str.size() << '\n';
    
}