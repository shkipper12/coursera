#include <iostream>
#include <map>
#include <string>

int main()
{
    int N;
    std::cin >> N;

    std::map<std::string, std::string> capitals{};

    while (N > 0) {
        
        std::string op;
        std::cin >> op;

        if (op == "CHANGE_CAPITAL") {
            std::string country, cpl;
            std::cin >> country >> cpl;
            if (capitals.count(country)) {
                if (capitals[country] == cpl) {
                    std::cout << "Country " << country << " hasn't changed its capital\n";
                }
                else {
                    std::cout << "Country " << country << " has changed its capital from " << capitals[country] << " to " << cpl << "\n";
                }
            }
            else {
                std::cout << "Introduce new country " << country << " with capital " << cpl << "\n";
            }
            capitals[country] = cpl;
        }
        else if (op == "RENAME") {
            std::string old_name, new_name;
            std::cin >> old_name >> new_name;
            if (old_name == new_name || !capitals.count(old_name) || capitals.count(new_name)) {
                std::cout << "Incorrect rename, skip\n";
            }
            else {
                std::string cpl = capitals[old_name];
                capitals.erase(old_name);
                capitals[new_name] = cpl;
                std::cout << "Country " << old_name << " with capital " << cpl << " has been renamed to " << new_name << "\n";
            }

        }
        else if (op == "ABOUT") {
            std::string country;
            std::cin >> country;
            if (capitals.count(country)) {
                std::cout << "Country " << country << " has capital " << capitals[country] << "\n";
            }
            else {
                std::cout << "Country " << country << " doesn't exist\n";
            }
        }
        else if (op == "DUMP") {
            if (capitals.size()) {
                for (const auto& [key, value] : capitals) {
                    std::cout << key << '/' << value << ' ';
                }
                std::cout << '\n';
            }
            else {
                std::cout << "There are no countries in the world\n";
            }
        }
        else
            std::cout << "Unknown Command\n";
        
        --N;
    }
}