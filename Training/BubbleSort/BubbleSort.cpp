#include <vector>
#include <iostream>

void BubbleSort(std::vector<int>& v)
{
    for (int i = 0; i < v.size()-1; ++i) {
        for (int y = i; y + 1 > 0; --y) {
            if (v[y+1] < v[y]) {
                std::swap(v[y+1], v[y]);
            }
        }
    }
}


int main()
{
    std::vector<int> vec;

    vec.push_back(2);
    vec.push_back(5);
    vec.push_back(1);
    vec.push_back(20);
    vec.push_back(0);
    vec.push_back(6);
    vec.push_back(7);


    BubbleSort(vec);

    for (const int& v : vec)
    {
        std::cout << v << ' ';
    }
}