#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

std::string to_lower(const std::string& s) {
    std::string res;
    for (const auto c : s) {
        res += std::tolower(c);
    }
    return res;
}

int main()
{
    int N;
    std::cin >> N;

    std::vector<std::string> arr(N);

    for (int i = 0; i < N; ++i) {
        std::cin >> arr[i];
    }

    std::sort(std::begin(arr), std::end(arr), [](std::string& s1, std::string& s2) {
        return to_lower(s1) <= to_lower(s2);
    });


    for (const auto& s : arr) {
        std::cout << s << ' ';
    }

}