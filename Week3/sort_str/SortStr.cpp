#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

class SortedStrings {
public:
    void AddString(const std::string& s) {
    // добавить строку s в набор
        sorted.push_back(s);
        Sort();
    }
    std::vector<std::string> GetSortedStrings() {
    // получить набор из всех добавленных строк в отсортированном порядке
        return sorted;
    }
private:
    // приватные поля
    void Sort() {
        std::sort(std::begin(sorted), std::end(sorted));
    }

    std::vector<std::string> sorted;
};

void PrintSortedStrings(SortedStrings& strings) {
    for (const std::string& s : strings.GetSortedStrings()) {
        std::cout << s << " ";
    }
    std::cout << '\n';
}

int main() 
{
    SortedStrings strings;
  
    strings.AddString("first");
    strings.AddString("third");
    strings.AddString("second");
    PrintSortedStrings(strings);

    strings.AddString("second");
    PrintSortedStrings(strings);
}