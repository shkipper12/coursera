#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <algorithm>

struct PersonInfo
{
    std::string first_name = "unknown";
    std::string last_name = "unknown";
};

class Person 
{
public:

    void ChangeFirstName(int year, const std::string& first_name) {
    // добавить факт изменения имени на first_name в год year
        PersonInfo info = GetRelativeInfo(year);

        m_history[year].first_name = first_name;
        m_history[year].last_name = info.last_name;

        UpdateInfoForFutherYears(year, info, m_history[year]);
    }

    void ChangeLastName(int year, const std::string& last_name) {
    // добавить факт изменения фамилии на last_name в год year
        PersonInfo info = GetRelativeInfo(year);
        m_history[year].last_name = last_name;
        m_history[year].first_name = info.first_name;

        UpdateInfoForFutherYears(year, info, m_history[year]);

    }
    std::string GetFullName(int year) {
    // получить имя и фамилию по состоянию на конец года year
        PersonInfo info = GetRelativeInfo(year);
        std::string res = "Incognito";
        if (m_history.size() > 0) {
            if (info.first_name == "unknown" && info.last_name == "unknown") {
                res = "Incognito";
            }
            else if (info.first_name == "unknown") {
                res = info.last_name + " with unknown first name";
            }
            else if (info.last_name == "unknown") {
                res = info.first_name + " with unknown last name";
            }
            else {
                res = info.first_name + " " + info.last_name;
            }
        }
        return res;
    }

private:
    std::map<int, PersonInfo> m_history;
    
    int GetRelatedYear(int search_year) {
        int last_year = 0;
        if (m_history.count(search_year)) {
            last_year = search_year;
        }
        else {
            for (const auto& [year, info] : m_history) {
                if (search_year < year) {
                    break;
                }
                else {
                    last_year = year;
                }
            }
        }

        return last_year;
    }

    PersonInfo GetRelativeInfo(int searh_year) {
        int last_year = GetRelatedYear(searh_year);
        return m_history[last_year];
    }

    void UpdateInfoForFutherYears(int year, PersonInfo& old_info, PersonInfo& new_info) {
        for (auto& [y, v] : m_history) {
            if (y > year) {
                
                if (v.first_name == old_info.first_name) {
                    v.first_name = new_info.first_name;
                }

                if (v.last_name == old_info.last_name) {
                    v.last_name = new_info.last_name;
                }
            }
        }
    }
};

int main()
{
    Person person;
  
    person.ChangeFirstName(1965, "Polina");
    person.ChangeLastName(1967, "Sergeeva");
    for (int year : {1900, 1965, 1990}) {
        std::cout << person.GetFullName(year) << std::endl;
    }

    person.ChangeFirstName(1970, "Appolinaria");
    for (int year : {1969, 1970}) {
        std::cout << person.GetFullName(year) << std::endl;
    }

    person.ChangeLastName(1968, "Volkova");
    for (int year : {1969, 1970}) {
        std::cout << person.GetFullName(year) << std::endl;
    }


    

}