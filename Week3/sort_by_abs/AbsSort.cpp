#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> arr(N);

    for (int i = 0; i < N; ++i) {
        std::cin >> arr[i];
    }

    std::sort(std::begin(arr), std::end(arr), [](int a, int b) {
        return std::abs(a) <= std::abs(b);
    });

    for (const auto& x : arr) { 
        std::cout << x << ' ';
    }

}