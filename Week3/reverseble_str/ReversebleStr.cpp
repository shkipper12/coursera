#include <string>
#include <vector>
#include <vector>
#include <iostream>
#include <algorithm>

class ReversibleString
{
public:
    ReversibleString() {
        m_string = "";
    }

    ReversibleString(const char* str){
        m_string = str;
    }

    ReversibleString(const std::string& str){
        m_string = str;
    }

    void Reverse() {
        std::reverse(std::begin(m_string), std::end(m_string));
    }

    std::string ToString() const {
        return m_string;
    }

private:
    std::string m_string;
};

int main()
{
    ReversibleString s("live");
    s.Reverse();
    std::cout << s.ToString() << std::endl;
  
    s.Reverse();
    const ReversibleString& s_ref = s;
    std::string tmp = s_ref.ToString();
    std::cout << tmp << std::endl;
    
    ReversibleString empty;
    std::cout << '"' << empty.ToString() << '"' << std::endl;

    std::string sss = "123";
    ReversibleString s2(sss);
    s2.Reverse();
    std::cout << '"' << s2.ToString() << '"' << std::endl;
}