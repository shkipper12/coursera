#include <iostream>
#include <vector>
#include <string>

int main()
{
    int N;
    std::cin >> N;

    std::vector<int> binary;
    if (N == 0)
        std::cout << 0;
    else {
        while(N != 1) {
            int bin = N % 2; // получаем остаток
            binary.push_back(bin);
            N /= 2; // делин на два
        }
        binary.push_back(1); // добавляем последнее значение

        std::string res;
        for (auto x : binary){
            res =  std::to_string(x) + res;
        }
        std::cout << res;
    }

}