#include <iostream>

int main()
{
    int A, B;
    std::cin >> A >> B;

    /* base solution
    if (A == B) {
        std::cout << A;
    }
    else if (A < B) {
        for (int t = A; t > 0; --t) {
            if ((A % t == 0) && (B % t == 0)){
                std::cout << t;
                break;
            }
        }
    }
    else {
        for (int t = B; t > 0; --t) {
            if ((A % t == 0) && (B % t == 0)){
                std::cout << t;
                break;
            }
        }
    }

    */

    // алгоритм Евклида
    while (A > 0 && B > 0) {
        if (A > B)
            A %= B;
        else
            B %= A;
    }

    std::cout << A + B;
}