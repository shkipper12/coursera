// Реализуйте функции и методы классов и при необходимости добавьте свои
#include <string>
#include <map>
#include <set>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

class Date 
{
public:
    Date() 
    {
        m_Year = 0;
        m_Month = 0;
        m_Day = 0;
    }

    Date(int year, int month, int day)
    {
        m_Year - year;
        m_Month = month;
        m_Day = day;

        ValidateDate();
    }

    Date(string date)
    {
        // input format 'y-m-d'
        stringstream ss;
        ss << date;

        bool res = false;
        
        // read year
        res = (ss >> m_Year && (ss.peek() == '-')) ? true : false;
        ss.ignore(1);

        // read month
        if (res) {
            res = (ss >> m_Month && (ss.peek() == '-')) ? true : false;
            ss.ignore(1);
        }
        // read day
        if (res) {
            res = (ss >> m_Day && ss.eof()) ? true : false;
        }

        if (!res) {
            throw invalid_argument("Wrong date format: " + date);
        }

        ValidateDate();
    }

    int GetYear() const 
    {
        return m_Year;
    }
    int GetMonth() const 
    {
        return m_Month;
    }
    int GetDay() const 
    {
        return m_Day;
    }

private:

    void ValidateDate()
    {
        if (m_Month < 1 || m_Month > 12) {
            throw invalid_argument("Month value is invalid: " + to_string(m_Month));
        }
        else if (m_Day < 1 || m_Day > 31) {
            throw invalid_argument("Day value is invalid: " + to_string(m_Day));
        }
    }

    int m_Year;
    int m_Month;
    int m_Day;
};

bool operator<(const Date& lhs, const Date& rhs) 
{
    return (lhs.GetDay() + lhs.GetMonth()*12 + lhs.GetYear()*31) < (rhs.GetDay() + rhs.GetMonth()*12 + rhs.GetYear()*31);
}

istream& operator>>(istream& in, Date& lhs)
{
    
    string date;
    in >> date;
    lhs = {date};
    return in;
}

class Database 
{
public:
    void AddEvent(const Date& date, const string& event)
    {
        // add event to db
        m_Events[date].insert(event);
    }

    bool DeleteEvent(const Date& date, const string& event)
    {
        // delete event at date from db
        
        if (m_Events.count(date) && m_Events[date].count(event)) {
           
            m_Events[date].erase(event);
            cout << "Deleted successfully\n";
            return true;
        }
        else {
            cout << "Event not found\n";
            return false;
        }
    }

    int DeleteDate(const Date& date)
    {
        // delete all events at date from db
        int n = 0;
        if (m_Events.count(date)) {
            n = m_Events[date].size();
            m_Events[date].clear();
        }
        cout << "Deleted " << n << " events\n";
        return n;
    }

    void Find(const Date& date) const
    {
        if (m_Events.count(date) && m_Events.at(date).size() > 0) {
            int i = 0;
            for (const auto& event : m_Events.at(date)) {
                cout << event << '\n';;
            }
        }
    }
    
    void Print() const
    {
        for (const auto& [date, events] : m_Events) {
            stringstream ss;
            ss << setfill('0') << setw(4) << date.GetYear() << '-';
            ss << setfill('0') << setw(2) << date.GetMonth() << '-';
            ss << setfill('0') << setw(2) << date.GetDay();

            for (const auto& ev : events) {
                cout << ss.str() << ' ' << ev << '\n';
            }
        }
    }

private:
    map<Date, set<string>> m_Events;
};

void Interact(istream& in, Database& db)
{
    string cmd;
    Date date;
    string event;
    
    in >> cmd; 
    if (cmd == "Add") {
        in >> date >> event;
        db.AddEvent(date, event);
    }
    else if (cmd == "Del") {
        in >> date >> event;
        if (event.size()){
            db.DeleteEvent(date, event);
        }
        else {
            db.DeleteDate(date);
        }
    }
    else if (cmd == "Find") {
        in >> date;
        db.Find(date);
    }
    else if (cmd == "Print") {
        db.Print();
    }
    else {
        cout << "Unknown command: " << cmd << '\n';
    }
}

int main() {

    Database db;
    string command;

    while (getline(cin, command)) 
    {
        // check for empty input
        if (!command.size()){
            continue;
        }

        // Считайте команды с потока ввода и обработайте каждую
        stringstream ss;
        ss << command;
        
        try {
            Interact(ss, db);
        }
        catch (invalid_argument& ex) {
            cout << ex.what() << '\n';
        }
        catch (exception& ex) {
            cout << ex.what() << '\n';
        }
    }

    return 0;
}