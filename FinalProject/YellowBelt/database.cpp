#include "database.h"

#include <algorithm>

bool operator<(const std::pair<Date, std::string>& left, const std::pair<Date, std::string>& right)
{
	return left.first < right.first;
}

bool operator==(const std::pair<Date, std::string>& left, const std::pair<Date, std::string>& right)
{
	return left.first == right.first && right.second == left.second;
}

std::ostream& operator<<(std::ostream& os, const std::pair<Date, std::string>& pair_)
{
    return os << pair_.first << " " << pair_.second;
}

void Database::Print(std::ostream& out) const
{
    for (const auto& pair_ : storage_)
    {
        for (const auto& item : pair_.second)
        {
			out << pair_.first << " " << item << '\n';
        }
    }
}

void Database::Add(const Date& date, const std::string& event)
{  
    const size_t count = checker_[date].size();

	checker_[date].insert(event);

    if (checker_.at(date).size() > count)
    {
		storage_[date].push_back(event);
    }
}

std::pair<Date, std::string> Database::Last(const Date& date) const
{
	if (storage_.empty())
		throw std::invalid_argument("Empty storage");

	auto upper = storage_.upper_bound(date);

    if (upper == storage_.begin())
		throw std::invalid_argument("Less date");

	--upper;

	return make_pair(upper->first, upper->second.back());
} 