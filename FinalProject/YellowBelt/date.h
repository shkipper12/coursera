#pragma once

#include <string>
#include <iostream>

class Date
{
public:
    Date();
    Date(int year, int month, int day);
    Date(std::string date);

    int GetYear() const;
    int GetMonth() const;
    int GetDay() const;

    std::string tostring() const;

private:
    void ValidateDate();
    int m_Year;
    int m_Month;
    int m_Day;
};

bool operator<(const Date& lhs, const Date& rhs);
bool operator>(const Date& lhs, const Date& rhs);
bool operator<=(const Date& lhs, const Date& rhs);
bool operator>=(const Date& lhs, const Date& rhs);
bool operator==(const Date& lhs, const Date& rhs);
bool operator!=(const Date& lhs, const Date& rhs);

std::istream& operator>>(std::istream& in, Date& lhs);
std::ostream& operator<<(std::ostream& out, const Date& lhs);
Date ParseDate(std::istream& in);