#include "date.h"
#include <sstream>
#include <iomanip>

Date::Date() 
{
    m_Year = 1;
    m_Month = 1;
    m_Day = 1;
}

Date::Date(int year, int month, int day)
{
    m_Year = year;
    m_Month = month;
    m_Day = day;

    ValidateDate();
}

Date::Date(std::string date)
{
    // input format 'y-m-d'
    std::stringstream ss;
    ss << date;

    bool res = false;
    
    // read year
    res = (ss >> m_Year && (ss.peek() == '-')) ? true : false;
    ss.ignore(1);

    // read month
    if (res) {
        res = (ss >> m_Month && (ss.peek() == '-')) ? true : false;
        ss.ignore(1);
    }
    // read day
    if (res) {
        res = (ss >> m_Day && ss.eof()) ? true : false;
    }

    if (!res) {
        throw std::invalid_argument("Wrong date format: " + date);
    }

    ValidateDate();
}

int Date::GetYear() const 
{
    return m_Year;
}

int Date::GetMonth() const 
{
    return m_Month;
}

int Date::GetDay() const 
{
    return m_Day;
}

std::string Date::tostring() const 
{
    std::ostringstream ss; ss << *this;
    return ss.str();
}

void Date::ValidateDate()
{
    if (m_Month < 1 || m_Month > 12) {
        throw std::invalid_argument("Month value is invalid: " + std::to_string(m_Month));
    }
    else if (m_Day < 1 || m_Day > 31) {
        throw std::invalid_argument("Day value is invalid: " + std::to_string(m_Day));
    }
}

bool operator<(const Date& lhs, const Date& rhs)
{
    if (lhs.GetYear() < rhs.GetYear())
        return true;
    else if (lhs.GetYear() > rhs.GetYear())
        return false;
    if (lhs.GetMonth() < rhs.GetMonth())
        return true;
    else if (lhs.GetMonth() > rhs.GetMonth())
        return false;    
    if (lhs.GetDay() < rhs.GetDay())
        return true;
    return false;
}

bool operator==(const Date& lhs, const Date& rhs)
{
    return (lhs.GetDay() == rhs.GetDay()) && (lhs.GetMonth() && rhs.GetMonth()) && (lhs.GetYear() && rhs.GetYear());
}

bool operator>(const Date& lhs, const Date& rhs)
{
    return rhs < lhs;
}

bool operator<=(const Date& lhs, const Date& rhs)
{
    return (lhs < rhs) || (lhs == rhs);
}

bool operator>=(const Date& lhs, const Date& rhs)
{
    return (lhs > rhs) || (lhs == rhs);
}

bool operator!=(const Date& lhs, const Date& rhs)
{
    return !(rhs == lhs);
}

std::istream& operator>>(std::istream& in, Date& lhs)
{
    std::string date;
    in >> date;
    lhs = {date};
    return in;
}

std::ostream& operator<<(std::ostream& out, const Date& lhs)
{
    return out << std::setfill('0') << std::setw(4) << lhs.GetYear() << '-'
                << std::setfill('0') << std::setw(2) << lhs.GetMonth() << '-'
                << std::setfill('0') << std::setw(2) << lhs.GetDay();
}

Date ParseDate(std::istream& in)
{
    std::string date;
    in >> date;
    return {date};
}