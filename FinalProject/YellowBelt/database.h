#pragma once

#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <unordered_set>

#include "date.h"

class Database
{
public:
	Database() = default;

	void Add(const Date& date, const std::string& event);

	void Print(std::ostream& out) const;

    template<typename Predicate>
	int RemoveIf(const Predicate& predicate)
	{
		int result = 0;

		std::map<Date, std::vector<std::string>> new_storage;
		std::map<Date, std::unordered_set<std::string>> new_checker;

        for (auto& pair_ : storage_)
        {
            const auto border = stable_partition(pair_.second.begin(), pair_.second.end(), 
				[predicate, pair_](const auto& item)
            {
				return predicate(pair_.first, item);
			});

			const size_t tmp = pair_.second.size();

            if (border == pair_.second.end())
            {
				result += tmp;
            }
            else
            {
				new_storage[pair_.first] = std::vector<std::string>(border, pair_.second.end());
				new_checker[pair_.first] = std::unordered_set<std::string>(border, pair_.second.end());

				result += tmp - new_storage.at(pair_.first).size();
            }
        }

		storage_ = new_storage;
		checker_ = new_checker;

		return result;
    }

    template<typename Predicate>
	std::vector<std::pair<Date, std::string>> FindIf(const Predicate& predicate) const
    {
		std::map<Date, std::vector<std::string>> tmp;
		std::vector<std::pair<Date, std::string>> result;

        for (const auto& pair_ : storage_)
        {
			std::vector<std::string> tmp_vec;

            copy_if(pair_.second.begin(), pair_.second.end(), back_inserter(tmp_vec),
				[predicate, pair_](const auto& item)
			{
				return predicate(pair_.first, item);
			});

            if (tmp_vec.size() != 0)
            {
				tmp[pair_.first] = tmp_vec;
            }
        }

		for (const auto& pair_ : tmp)
		{
		    for (const std::string& item : pair_.second)
		    {
				result.push_back(make_pair(pair_.first, item));
		    }
		}

		return result;
    }

	std::pair<Date, std::string> Last(const Date& date) const;

private:
	std::map<Date, std::vector<std::string>> storage_;
	std::map<Date, std::unordered_set<std::string>> checker_;
};


std::ostream& operator<<(std::ostream& os, const std::pair<Date, std::vector<std::string>>& pair_);
bool operator<(const std::pair<Date, std::string>& left, const std::pair<Date, std::string>& right);
bool operator==(const std::pair<Date, std::string>& left, const std::pair<Date, std::string>& right);
std::ostream& operator<<(std::ostream& os, const std::pair<Date, std::string>& pair_);