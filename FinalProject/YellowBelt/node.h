#pragma once
#include "date.h"
#include <memory>

enum class Comparison
{
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
    Equal,
    NotEqual
};

enum class LogicalOperation
{
    And,
    Or
};

class Node
{
public:
    virtual bool Evaluate(const Date& d, const std::string& event) const = 0;
};

class EmptyNode : public Node
{
public:
    EmptyNode() {}
    bool Evaluate(const Date& d, const std::string& event) const override
    {
        return true;
    }
};

class DateComparisonNode : public Node
{
public:
    DateComparisonNode(Comparison cmp, const Date& d)
        : m_Cmp(cmp), m_Date(d)
    {}

    bool Evaluate(const Date& d, const std::string& event) const override;
private:
    const Comparison m_Cmp;
    const Date m_Date;
};

class EventComparisonNode : public Node
{
public:
    EventComparisonNode(Comparison cmp, const std::string& e)
        : m_Cmp(cmp), m_Event(e)
    {}

    bool Evaluate(const Date& d, const std::string& event) const override;
private:
    const Comparison m_Cmp;
    const std::string m_Event;
};

class LogicalOperationNode : public Node
{
public:
    LogicalOperationNode(LogicalOperation op, const std::shared_ptr<Node>& lhs, const std::shared_ptr<Node>& rhs)
        : m_Op(op), m_Left(lhs), m_Right(rhs)
    {}

    bool Evaluate(const Date& d, const std::string& event) const override;
private:
    const LogicalOperation m_Op;
    const std::shared_ptr<Node> m_Left;
    const std::shared_ptr<Node> m_Right;
};