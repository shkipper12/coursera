#include "node.h"

template<class T>
bool compare(Comparison cmp, const T& a, const T& b)
{
    switch (cmp)
    {
        case Comparison::Less:
            return a < b;

        case Comparison::LessOrEqual:
            return a <= b;

        case Comparison::Greater:
            return a > b;

        case Comparison::GreaterOrEqual:
            return a >= b;

        case Comparison::Equal:
            return a == b;

        case Comparison::NotEqual:
            return a != b;
    }

    throw std::invalid_argument("Invalid comparison operator");
}

bool DateComparisonNode::Evaluate(const Date& date, const std::string& event) const
{
    bool res = compare(m_Cmp, date, m_Date);
    return res;
}

bool EventComparisonNode::Evaluate(const Date& date, const std::string& event) const
{
    return compare(m_Cmp, event, m_Event);
}

bool LogicalOperationNode::Evaluate(const Date& date, const std::string& event) const
{
    switch(m_Op)
    {
        case LogicalOperation::Or:
            return m_Left->Evaluate(date, event) || m_Right->Evaluate(date, event);

        case LogicalOperation::And:
            return m_Left->Evaluate(date, event) && m_Right->Evaluate(date, event);
    }

    throw std::invalid_argument("Invalid logical operation");
}