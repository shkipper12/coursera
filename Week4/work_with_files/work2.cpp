#include <string>
#include <fstream>
#include <iostream>

int main()
{
    //std::ifstream input("E:\\Documents\\Programing\\VS_Projects\\coursera\\Builds\\input.txt");
    std::ifstream input("input.txt");
    //std::ofstream output("E:\\Documents\\Programing\\VS_Projects\\coursera\\Builds\\output.txt");
    std::ofstream output("output.txt");

    if (input.is_open()){
        std::string line;
        while (std::getline(input, line))
        {
            output << line << std::endl;
        }
    }
}