#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

int main()
{
    //std::ifstream input("E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\work_with_files\\input2.txt");
    std::ifstream input("input.txt");
    
    if (input.is_open()){
        double value;
        std::cout << std::fixed << std::setprecision(3);
        while(input >> value){
            std::cout << value << '\n';
        }

    }
}