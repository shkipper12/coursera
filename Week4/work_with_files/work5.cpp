#include <iostream>
#include <string>
#include <vector>

struct Student
{
    std::string name;
    std::string surname;
    int day;
    int month;
    int year;
};

void PrintFullName(const Student& s) {
    std::cout << s.name << ' ' << s.surname << '\n';
}

void PrintDateOfBirth(const Student& s) {
    std::cout << s.day << '.' << s.month << '.' << s.year << '\n';
}

int main()
{
    int N, i;
    std::cin >> N;
    i = N;

    std::vector<Student> group;

    while (i > 0)
    {
        std::string name, surname;
        int d, m, y;
        std::cin >> name >> surname >> d >> m >> y;
        group.push_back({name, surname, d, m, y});
        --i;
    }

    int M;
    std::cin >> M;

    while (M > 0)
    {
        std::string op;
        int k;
        std::cin >> op >> k;
        if (k > N || k < 1) {
            std::cout << "bad request\n";
        }
        else if (op == "name"){
            PrintFullName(group[k-1]);
        }
        else if (op == "date") {
            PrintDateOfBirth(group[k-1]);
        }
        else {
            std::cout << "bad request\n";
        }
        --M;
    }
    
    
}