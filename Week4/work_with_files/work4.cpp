#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

int main()
{
    //std::ifstream input("E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\work_with_files\\input3.txt");
    std::ifstream input("input.txt");
    //std::ofstream output2("E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\work_with_files\\output3.txt");
    if (input.is_open()){

        int N = 0, M = 0;
        input >> N >> M;

        for (int i = 0; i < N; ++i) {
            for (int y = 0; y < M; ++y) {
                int v;
                input >> v;

                if(y != M-1) {// skip comma
                    input.ignore(1);
                    std::cout << std::setw(10) << v << ' ';
                }
                else {
                    if (i != N-1)
                        std::cout << std::setw(10) << v << '\n';
                    else
                        std::cout << std::setw(10) << v;
                }

            }
        }
    }
}