#include <iostream>
#include <sstream>
#include <numeric>
#include <string>
#include <vector>
#include <set>
#include <map>
using namespace std;


int GetGCD(int A, int B)
{
    // std method
    //return std::gcd(numerator, denominator);

    // алгоритм Евклида
    while (A > 0 && B > 0) {
        if (A > B)
            A %= B;
        else
            B %= A;
    }

    return A + B;
}

int GetLCM(int A, int B)
{
    return (A*B)/GetGCD(A,B);
}

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
        m_num = 0;
        m_denom = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
        if (denominator == 0) {
            throw invalid_argument("Invalid argument");
        }

        if (numerator < 0 && denominator < 0) {
            numerator *= -1;
            denominator *= -1;
            Reduce(numerator, denominator);
        }
        if (numerator < 0) {
            numerator *= -1;
            Reduce(numerator, denominator);
            m_num *= -1;
        }
        else if (denominator < 0) {
            denominator *= -1;
            Reduce(numerator, denominator);
            m_num *= -1;
        }
        else {
            Reduce(numerator, denominator);
        }
    }

    int Numerator() const {
        // Реализуйте этот метод
        return m_num;
    }

    int Denominator() const {
        // Реализуйте этот метод
        return m_denom;
    }

    friend istream& operator>>(istream& in, Rational& lhs);

private:

    void Reduce(int numerator, int denominator) 
    {
        int f = GetGCD(numerator, denominator);
        m_num = numerator / f;
        m_denom = denominator / f;
    }

    // Добавьте поля
    int m_num;
    int m_denom;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(const Rational& lhs, const Rational& rhs)
{
    return (lhs.Numerator() == rhs.Numerator()) && (lhs.Denominator() == rhs.Denominator());
}

Rational operator*(const Rational& lhs, int a)
{
    return {lhs.Numerator() * a, lhs.Denominator()};
}


Rational operator*(const Rational& lhs, const Rational& rhs)
{
    return {lhs.Numerator() * rhs.Numerator(), lhs.Denominator() * rhs.Denominator()};
}

Rational operator/(const Rational& lhs, const Rational& rhs)
{
    if (rhs.Numerator() == 0) {
        throw domain_error("Division by zero");
    }
    return {lhs.Numerator() * rhs.Denominator(), lhs.Denominator() * rhs.Numerator()};
}

Rational operator+(const Rational& lhs, const Rational& rhs)
{
    int lcm = GetLCM(lhs.Denominator(), rhs.Denominator());
    int A1 = lhs.Numerator() * (lcm / lhs.Denominator());
    int A2 = rhs.Numerator() * (lcm / rhs.Denominator());

    return {A1 + A2, lcm};
}

Rational operator-(const Rational& lhs, const Rational& rhs)
{
    return lhs + rhs * -1;
}

bool operator<(const Rational& lhs, const Rational& rhs)
{
    int lcm = GetLCM(lhs.Denominator(), rhs.Denominator());
    int A1 = lhs.Numerator() * (lcm / lhs.Denominator());
    int A2 = rhs.Numerator() * (lcm / rhs.Denominator());

    return A1 < A2;
}

ostream& operator<<(ostream& out, const Rational& lhs)
{
    out << lhs.Numerator() << '/' << lhs.Denominator();
    return out;
}

istream& operator>>(istream& in, Rational& lhs)
{
    int p = -1, q = -1;
    char op;
    if (in >> p && in >> op && in >> q) {
        if (op == '/')
            lhs = { p, q };
    }
   
    
    return in;
}

int main() 
{
    {
        const Rational r(3, 10);
        if (r.Numerator() != 3 || r.Denominator() != 10) {
            cout << "Rational(3, 10) != 3/10" << endl;
            return 1;
        }
    }

    {
        const Rational r(8, 12);
        if (r.Numerator() != 2 || r.Denominator() != 3) {
            cout << "Rational(8, 12) != 2/3" << endl;
            return 2;
        }
    }

    {
        const Rational r(-4, 6);
        if (r.Numerator() != -2 || r.Denominator() != 3) {
            cout << "Rational(-4, 6) != -2/3" << endl;
            return 3;
        }
    }

    {
        const Rational r(4, -6);
        if (r.Numerator() != -2 || r.Denominator() != 3) {
            cout << "Rational(4, -6) != -2/3" << endl;
            return 3;
        }
    }

    {
        const Rational r(0, 15);
        if (r.Numerator() != 0 || r.Denominator() != 1) {
            cout << "Rational(0, 15) != 0/1" << endl;
            return 4;
        }
    }

    {
        const Rational defaultConstructed;
        if (defaultConstructed.Numerator() != 0 || defaultConstructed.Denominator() != 1) {
            cout << "Rational() != 0/1" << endl;
            return 5;
        }
    }

    {
        Rational r1(4, 6);
        Rational r2(2, 3);
        bool equal = r1 == r2;
        if (!equal) {
            cout << "4/6 != 2/3" << endl;
            return 1;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a + b;
        bool equal = c == Rational(2, 1);
        if (!equal) {
            cout << "2/3 + 4/3 != 2" << endl;
            return 2;
        }
    }

    {
        Rational a(5, 7);
        Rational b(2, 9);
        Rational c = a - b;
        bool equal = c == Rational(31, 63);
        if (!equal) {
            cout << "5/7 - 2/9 != 31/63" << endl;
            return 3;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a * b;
        bool equal = c == Rational(8, 9);
        if (!equal) {
            cout << "2/3 * 4/3 != 8/9" << endl;
            return 1;
        }
    }

    {
        Rational a(5, 4);
        Rational b(15, 8);
        Rational c = a / b;
        bool equal = c == Rational(2, 3);
        if (!equal) {
            cout << "5/4 / 15/8 != 2/3" << endl;
            return 2;
        }
    }

        {
        ostringstream output;
        output << Rational(-6, 8);
        if (output.str() != "-3/4") {
            cout << "Rational(-6, 8) should be written as \"-3/4\"" << endl;
            return 1;
        }
    }

    {
        istringstream input("5/7");
        Rational r;
        input >> r;
        bool equal = r == Rational(5, 7);
        if (!equal) {
            cout << "5/7 is incorrectly read as " << r << endl;
            return 2;
        }
    }

    {
        istringstream input("");
        Rational r;
        bool correct = !(input >> r);
        if (!correct) {
            cout << "Read from empty stream works incorrectly" << endl;
            return 3;
        }
    }

    {
        istringstream input("5/7 10/8");
        Rational r1, r2;
        input >> r1 >> r2;
        bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << endl;
            return 4;
        }

        input >> r1;
        input >> r2;
        correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Read from empty stream shouldn't change arguments: " << r1 << " " << r2 << endl;
            return 5;
        }
    }

    {
        istringstream input1("1*2"), input2("1/"), input3("/4");
        Rational r1, r2, r3;
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational() && r2 == Rational() && r3 == Rational();
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments: "
                 << r1 << " " << r2 << " " << r3 << endl;

            return 6;
        }
    }

    {
        istringstream input("2/4/6/8");
        Rational r1;
        input >> r1;
        bool correct = r1 == Rational(1, 2);
        if (!correct) {
            cout << "Multiple-slashed number sequence value read incorrectly: " << r1 << endl;
            return 7;
        }
    }

    {
        istringstream input("3/10");
        Rational r1;
        input >> r1;
        bool correct = r1 == Rational(3, 10);
        if (!correct) {
            cout << "Rational(3, 10) != 3/10" << endl;
            return 1;
        }
    }

    {
        istringstream input("-4/6");
        Rational r1;
        input >> r1;
        bool correct = r1 == Rational(-4, 6);
        if (!correct) {
            cout << "Rational(-4, 6) != -2/3" << endl;
            return 3;
        }
    }

    {
        istringstream input("4/-6");
        Rational r1;
        input >> r1;
        bool correct = r1 == Rational(4, -6);
        if (!correct) {
            cout << "Rational(4, -6) != -2/3" << endl;
            return 3;
        }
    }

    {
        istringstream input("0/15");
        Rational r1;
        input >> r1;
        bool correct = r1 == Rational(0, 15);
        if (!correct) {
            cout << "Rational(0, 15) != 0/1" << endl;
            return 4;
        }
    }

    {
        const set<Rational> rs = {{1, 2}, {1, 25}, {3, 4}, {3, 4}, {1, 2}};
        if (rs.size() != 3) {
            cout << "Wrong amount of items in the set" << endl;
            return 1;
        }

        vector<Rational> v;
        for (auto x : rs) {
            v.push_back(x);
        }
        if (v != vector<Rational>{{1, 25}, {1, 2}, {3, 4}}) {
            cout << "Rationals comparison works incorrectly" << endl;
            return 2;
        }
    }

    {
        map<Rational, int> count;
        ++count[{1, 2}];
        ++count[{1, 2}];

        ++count[{2, 3}];

        if (count.size() != 2) {
            cout << "Wrong amount of items in the map" << endl;
            return 3;
        }
    }

    try {
        Rational r(1, 0);
        cout << "Doesn't throw in case of zero denominator" << endl;
        return 1;
    } catch (invalid_argument& ex) {
        //std::cout << ex.what() << '\n';
    }

    try {
        auto x = Rational(1, 2) / Rational(0, 1);
        cout << "Doesn't throw in case of division by zero" << endl;
        return 2;
    } catch (domain_error& ex) {
        //cout << ex.what() << '\n';
    }

    //cout << "OK" << endl;
    
    {
        Rational r1, r2;
        char op;
        try {
            cin >> r1 >> op >> r2;
            if (op == '+') {
                cout << r1 + r2 << '\n';
            }
            else if (op == '-') {
                cout << r1 - r2 << '\n';
            }
            else if (op == '/') {
                cout << r1 / r2 << '\n';
            }
            else if (op == '*') {
                cout << r1 * r2 << '\n';
            }
            else {
                cout << "Unknown operation\n";
            }
        }catch (exception& ex) {
            std::cout << ex.what() << '\n';
        }
    }
    
    return 0;
}