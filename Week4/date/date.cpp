#include <iostream>
using namespace std;

struct Day
{
    int value;
    explicit Day(int new_val) {
        value = new_val;
    }
};

struct Month
{
    int value;
    explicit Month(int new_val) {
        value = new_val;
    }
};

struct Year
{
    int value;
    explicit Year(int new_val) {
        value = new_val;
    }
};

struct Date 
{
    int day;
    int month;
    int year;

    Date(Day new_day, Month new_month, Year new_year)
    {
        day = new_day.value;
        month = new_month.value;
        year = new_year.value;
    }
};

int main()
{
    Date d = {Day{1}, Month{1}, Year{12}};
    Date d2 = {Day(2), Month(3), Year(17)};
    Date d4(Day(4), Month(5), Year(19));
}