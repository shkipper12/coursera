#include <string>
using namespace std;

struct Specialization
{
    string value;
    explicit Specialization(const string& s) {
        value = s;
    }
};

struct Course
{
    string value;
    explicit Course(const string& s) {
        value = s;
    }
};

struct Week
{
    string value;
    explicit Week(const string& s) {
        value = s;
    }
};


struct LectureTitle 
{
    string specialization;
    string course;
    string week;

    LectureTitle(Specialization spec, Course crs, Week wk)
    {
        specialization = spec.value;
        course = crs.value;
        week = wk.value;
    }
};


int main()
{
    LectureTitle title(Specialization("C++"), 
        Course("White belt"), Week("4th"));

    LectureTitle d = {Specialization{"C++"}, Course{"White belt"}, Week{"4th"}};
    //LectureTitle title("C++", "White belt", "4th");
    //LectureTitle title(string("C++"), string("White belt"), string("4th"));
    //LectureTitle title = {"C++", "White belt", "4th"};
    //LectureTitle title = {{"C++"}, {"White belt"}, {"4th"}};
    // LectureTitle title(Course("White belt"), 
    //     Specialization("C++"), Week("4th"));

    // LectureTitle title(Specialization("C++"),
    //     Week("4th"), Course("White belt"));

}