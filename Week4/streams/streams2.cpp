#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

void PrintFormated(const vector<string>& names, const vector<double>& values, int width)
{
    for (const auto& n : names) {
        cout << setw(width) << n << ' ';
    }
    cout << endl;

    cout << fixed << setprecision(2);
    
    for (const auto& v : values) {
        cout << setw(width) << v << ' ';
    }
    cout << endl;
}

int main()
{
    vector<string> names = {"a", "b", "c"};
    vector<double> values = {5, 0.01, 0.000005};
    cout << setfill('.') << left;
    PrintFormated(names, values, 5);
}