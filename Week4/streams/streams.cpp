#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void ReadAll(const string path) 
{
    ifstream input3(path);
    if (input3) {
        string line;
        while (getline(input3, line)) {
            cout << line << endl;
        }
    }
}

int main()
{
    ifstream input("E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\streams\\date.txt");
    if (input) {
        string year, month, day;
        getline(input, year, '-');
        getline(input, month, '-');
        getline(input, day, '-');

        std::cout << year << ' ' << month << ' ' << day << endl;
    }
    else {
        cout << "File not exist\n";
    }

    ifstream input2("E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\streams\\date.txt");
    if (input2) {
        int year = 0, month = 0, day = 0;
        input2 >> year;
        input2.ignore(1);
        input2 >> month;
        input2.ignore(1);
        input2 >> day;
        input2.ignore(1);

        std::cout << year << ' ' << month << ' ' << day << endl;
    }
    else {
        cout << "File not exist\n";
    }

    const string path = "E:\\Documents\\Programing\\VS_Projects\\coursera\\Week4\\streams\\out.txt";
    ofstream output(path);
    output << "hello" << endl;

    fstream output2(path, ios::app); // не переписывает файл
    output2 << " world" << endl;
    ReadAll(path);
   
}