#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

vector<string> FormNamesHistory(int year, const map<int, string>& data)
{
	vector<string> result;
	int limit = data.begin() -> first;
	for(int i = year; i >= limit; --i) {
  		if(data.count(i))
 			result.push_back(data.at(i));
  	}
  	return result;
}

string FindName(const int year, const map<int, string>& data)
{
  	auto iter_after = data.upper_bound(year);
    string name;
    if (iter_after != data.begin()) {
        name = prev(iter_after)->second;
    }
    return name;
}

string GetHistoryNames(const vector<string>& data)
{
	string result;// = " (";
	string check = data[0];
	unsigned int limit = data.size()-1;
	for(unsigned int i = 1; i < limit; ++i)	{
		if(check != data[i]) {
			result += data[i] + ", ";
			check = data[i];
		}
	}

	if(data[limit] != check)
		result += data[limit];

	if(!result.empty()) {
		return " (" + result + ")";
	}
	return result;
}

class Person 
{
public:
    Person()
    {
        year_of_birth = 0;
        firstName[year_of_birth] = "";
        lastName[year_of_birth ] = "";
    }
    Person(const string& name, const string& surname, int year) {
        year_of_birth = year;
        firstName[year] = name;
        lastName[year] = surname;
    }

    void ChangeFirstName(int year, const string& first_name) 
    {
        // добавить факт изменения имени на first_name в год year
        if (year >= GetYearOfBirth()) {
            firstName[year] = first_name;
        }
    }


    void ChangeLastName(int year, const string& last_name)
    {
        // добавить факт изменения фамилии на last_name в год year
        if (year >= GetYearOfBirth()) {
            lastName[year] = last_name;
        }
    }

    string GetFullName(int year) const {
    // получить имя и фамилию по состоянию на конец года year
        if (year < GetYearOfBirth()) {
            return "No person";
        }
    
        string name = FindName(year, firstName);
        string surname = FindName(year, lastName);

        if(!name.empty() && !surname.empty())
            return name + " " + surname;
        else if(!name.empty())
            return name + " with unknown last name";
        else if(!surname.empty())
            return surname + " with unknown first name";
        else return "Incognito";
    }

    string GetFullNameWithHistory(int year) const {
        if (year < GetYearOfBirth()) {
            return "No person";
        }
        
        vector<string> names = FormNamesHistory(year, firstName);
        vector<string> surnames = FormNamesHistory(year, lastName);
        string result;

        if(!names.empty() && !surnames.empty())	{
            result = names[0];

            if(names.size() > 1) {
                result += GetHistoryNames(names);
            }

            result += (" " + surnames[0]);

            if(surnames.size() > 1) {
                result += GetHistoryNames(surnames);
            }
            return result;
        }
        else if(!names.empty()) {
            result = names[0];

            if(names.size() > 1) {
                result += GetHistoryNames(names);
            }
            result += " with unknown last name";
        }
        else if(!surnames.empty()) {
            result = surnames[0];
            if(surnames.size() > 1) {
                result += GetHistoryNames(surnames);
            }
            result += " with unknown first name";
        }
        else result = "Incognito";

        return result;
    }

    int GetYearOfBirth() const {
        return year_of_birth;
    }

private:
    // приватные поля
	map<int, string> firstName;
	map<int, string> lastName;
    int year_of_birth;
};


int main() {
  Person person;
  
  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year : {1900, 1965, 1990}) {
    cout << person.GetFullName(year) << endl;
  }
  
  person.ChangeFirstName(1970, "Appolinaria");
  for (int year : {1969, 1970}) {
    cout << person.GetFullName(year) << endl;
  }
  
  person.ChangeLastName(1968, "Volkova");
  for (int year : {1969, 1970}) {
    cout << person.GetFullName(year) << endl;
  }
  
  return 0;
}