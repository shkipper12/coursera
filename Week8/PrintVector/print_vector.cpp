#include <algorithm>
#include <iostream>
#include <vector>

void PrintVectorPart(const std::vector<int>& numbers)
{
    auto first_negative = std::find_if(std::begin(numbers), std::end(numbers), [](int i){
        return i < 0;
    });

    auto it = first_negative;
    while (it != std::begin(numbers))
    {
        std::cout << *(--it) << ' ';
    }
}

int main()
{
    PrintVectorPart({6, 1, 8, -5, 4});
    std::cout << '\n';
    PrintVectorPart({-6, 1, 8, -5, 4});  // ничего не выведется
    std::cout << '\n';
    PrintVectorPart({6, 1, 8, 5, 4});
    std::cout << '\n';
    return 0;
}