#include <set>
#include <iostream>

using namespace std;

set<int>::const_iterator FindNearestElement(const set<int>& numbers, int border)
{
    set<int>::const_iterator nearest = numbers.lower_bound(border);
    if (nearest == numbers.end() && nearest != numbers.begin())
    {
        return prev(nearest);
    }
    else if (*nearest == border || nearest == numbers.begin()) {
        return nearest;
    }
    else {
        if ((border - *prev(nearest)) <= (*nearest - border))
        {
            return prev(nearest);
        }
        else
        {
            return nearest;
        }
    }
}
// set<int>::const_iterator —
// тип итераторов для константного множества целых чисел

int main() {
    set<int> numbers = {1, 4, 6};
    cout <<
        *FindNearestElement(numbers, 0) << " " <<
        *FindNearestElement(numbers, 3) << " " <<
        *FindNearestElement(numbers, 5) << " " <<
        *FindNearestElement(numbers, 6) << " " <<
        *FindNearestElement(numbers, 100) << endl;
        
    set<int> empty_set;

    cout << (FindNearestElement(empty_set, 8) == end(empty_set)) << endl;
return 0;
}