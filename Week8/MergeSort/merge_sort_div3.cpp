#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if(distance(range_begin, range_end) < 2) return;

    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    RandomIt it1 = elements.begin() + distance(elements.begin(), elements.end()) / 3;
    RandomIt it2 = it1 + distance(elements.begin(), elements.end()) / 3;
    MergeSort(elements.begin(), it1);
    MergeSort(it1, it2);
    MergeSort(it2, elements.end());
    vector<typename RandomIt::value_type> temp_merge;

    merge(elements.begin(), it1, it1, it2, back_inserter(temp_merge));
    merge(temp_merge.begin(), temp_merge.end(), it2, elements.end(), range_begin);
}

int main() {
    vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 5};
    MergeSort(begin(v), end(v));
    for (int x : v) {
        cout << x << " ";
    }
    cout << endl;
    return 0;
}