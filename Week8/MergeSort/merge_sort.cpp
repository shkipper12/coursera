#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if(distance(range_begin, range_end) < 2) return;

    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    RandomIt it1 = elements.begin() + distance(elements.begin(), elements.end()) / 2;
    MergeSort(elements.begin(), it1);
    MergeSort(it1, elements.end());
    merge(elements.begin(), it1, it1, elements.end(), range_begin);
}

int main() {
    vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1};
    MergeSort(begin(v), end(v));
    for (int x : v) {
        cout << x << " ";
    }
    cout << endl;
    return 0;
}