#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

vector<string> SplitIntoWords(const string& s)
{
    vector<string> res;
    auto start = begin(s);
    auto it = end(s);
    while ((it = find_if(start, end(s), [](char c){ return c == ' ';})) != end(s))
    {
        string word{start, it};
        res.push_back(word);
        start = (++it);
    }

    if (start != end(s)) {
        string word{start, end(s)};
        res.push_back(word);
    }

    return res;
    
}

int main() {
    string s = "C cpp";

    vector<string> words = SplitIntoWords(s);
    cout << words.size() << " ";
    for (auto it = begin(words); it != end(words); ++it) {
    if (it != begin(words)) {
        cout << "/";
    }
    cout << *it;
    }
    cout << endl;

    return 0;
}