#include <iostream>
#include <string>
#include <deque>

int main()
{
    int V, Q;
    std::cin >> V >> Q;
    std::deque<std::string> eq;
    eq.push_back(std::to_string(V));

    bool higher_op = true;
    while (Q > 0)
    {
        std::string op;
        int new_v;
        std::cin >> op >> new_v;

        if (op == "*" || op == "/")
        {
            if (!higher_op) {
                eq.push_front("(");
                eq.push_back(") " + op + " ");
            }
            else {
                eq.push_back(" " + op + " ");
            }

            higher_op = true;
        }
        else
        {
            eq.push_back(" " + op + " ");
            higher_op = false;
        }

        eq.push_back(std::to_string(new_v));

        --Q;
    }

    for (const auto& s : eq)
    {
        std::cout << s;
    }
}