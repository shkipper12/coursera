#include <iostream>
#include <string>
#include <deque>

int main()
{
    int V, Q;
    std::cin >> V >> Q;
    std::deque<std::string> eq;
    eq.push_back(std::to_string(V));

    while (Q > 0)
    {
        std::string op;
        int new_v;
        std::cin >> op >> new_v;

        eq.push_front("(");
        eq.push_back(") " + op + " ");
        eq.push_back(std::to_string(new_v));

        --Q;
    }

    for (const auto& s : eq)
    {
        std::cout << s;
    }
}