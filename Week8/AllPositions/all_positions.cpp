#include <algorithm>
#include <iostream>
#include <vector>
#include <numeric>

std::ostream& operator<<(std::ostream& out, const std::vector<int>& v)
{
    bool first = true;
    for (auto it = v.rbegin(); it != v.rend(); ++it)
    {
        if (first) {
            out << *it;
            first = false;
        }
        else {
            out << ' ' << *it;
        }
    }
    return out;
}

int main()
{
    int N;
    
    std::cin >> N;
    std::vector<int> elems(N);
    std::iota(elems.begin(), elems.end(), 1); 
    
    do {
        std::cout << elems << '\n';
    } while(std::prev_permutation(elems.rbegin(), elems.rend()));
}